const { colors } = require('tailwindcss/defaultTheme');
const gold = {
	100: '#FFEA66',
	200: '#FFE74C',
	300: '#FFE333',
	400: '#FFE019',
	500: '#FFDD00',
	600: '#E5C600',
	700: '#CCB000',
	800: '#B29A00',
	900: '#998400',
};

module.exports = {
	prefix: 'tw-',
	theme: {
		extend: {
			screens: {
				xs: '300px',
				sm: '576px',
				md: '768px',
				lg: '992px',
				xl: '1200px',
				hd: '1440px'
			},
			colors: {
				primary: {
					100: '#E7B1B4',
					200: '#E19EA1',
					300: '#D98689',
					400: '#D0686C',
					500: '#C44247',
					600: '#B51319',
					700: '#910F14',
					800: '#740C10',
					900: '#5D0A0D',
				},
				gray: {
					100: '#f7fafc',
					200: '#f2f2f2',
					300: '#e2e8f0',
					400: '#cbd5e0',
					500: '#a0aec0',
					600: '#718096',
					700: '#4a5568',
					800: '#2d3748',
					900: '#1a202c',
				}
			},
			fontFamily: {
				sans: [
					'"Muli", sans-serif'
				]
			},
			fontSize: {
				'xxs': '0.65rem',
				'2xl': '1.375rem',
				'3xl': '1.5rem',
				'4xl': '1.625rem',
				'5xl': '1.75rem',
				'6xl': '1.875rem',
				'7xl': '2rem',
				'8xl': '3rem',
				'9xl': '4rem',
			},
			lineHeight: {
				loose: '1.7',
				xloose: '2'
			},
			minHeight: {
				xs: '24rem',
			},
			inset: {
				'1/2': '50%',
			},
			zIndex: {
				'-1': '-1',
				'999': '999',
				'1000': '1000'
			}
		}
	},
	variants: {
		display: ['responsive', 'hover', 'group-hover'],
		zIndex: ['responsive', 'hover'],
	},
	plugins: [
	],
	corePlugins: {
		container: false,
	}
}