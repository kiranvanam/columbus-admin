<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;


$factory->define(Collection::class, function (Faker $faker) {
    $name = $faker->word();
    $is_country_collection = $faker->boolean(50);
    $is_destination_collection = !$is_country_collection;
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'short_description' => $faker->text(100),
        'long_description' => $faker->text(500),
        'is_country_collection' => $is_country_collection,
        'is_destination_collection' => $is_destination_collection
    ];
});