<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\FAQ;
use App\FAQCategory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(FAQCategory::class, function(Faker $faker) {
    $category = $faker->sentence(3);
    return [
        'label' => $category,
        'slug' => str_slug($category),
    ];
});

$factory->define(FAQ::class, function(Faker $faker) {
    $categories = FAQCategory::all()->random(2)->pluck('id')->toArray();
    return [
        'question' => $faker->sentence(3),
        'answer' => $faker->text(1000),
        'categories' => $categories
    ];
});