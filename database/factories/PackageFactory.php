<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

function arrayToUnorderedList($faker, $noOfListItems = 10)
{
    $finalText = "<ul>";
    $list = $faker->sentences($noOfListItems, $asText = false);
    for ($i=0; $i < count($list) ; $i++) {
        $finalText .= "<li> $list[$i]</li>";
    }
    $finalText .= "</ul>";
    return $finalText;
}

function arrayToDiv($faker) {
    $finalText = "";
    $list = $faker->paragraphs(3);
    for ($i=0; $i < count($list) ; $i++) {
        $finalText .= "<div> $list[$i]</div>";
    }
    return $finalText;
}

function getItinerary($duration, $faker) {
    $itinerary = [];
    for($i=0; $i<$duration; $i++) {
        $day = [
            'title' => $faker->words(3),
            'description' =>$faker->text(400),
            'meals' => [
                [
                    'name' => 'Breakfast',
                ],
                [
                    'name' => 'Lunch',
                ],
                [
                    'name' => 'Dinner',
                ]
            ]
        ];
        array_push($itinerary, $day);
    }
    return $itinerary;
}

$factory->define(Model::class, function (Faker $faker) {
    $title = $faker->sentence(6);
    $tour_types = ['Customized Holiday', 'Group Tour (Incl. Flights)', 'Group Tour (Excl. Flights)', 'Activity'];
    $itinerary = getItinerary(mt_rand(4,10), $faker);
    $traveller_min_age = mt_rand(0, 80);
    $traveller_max_age = mt_rand($traveller_min_age, 100);
    return [
        'title' => $title,
        'slug' => str_slug($name),
        'tour_code' => strtoupper($faker->text(4)),
        'tour_type' => $tour_types[array_rand($tour_types)],
        'price_starts_from' => $faker->randomNumber(5),
        'overview' => $faker->text(400),
        'no_of_countries' => 1,
        'no_of_cities' => 5,
        'tour_duration' => 1,
        'banner_image' => "https://img.veenaworld.com/group-tours/india/leh-ladakh/lhhl-i/lhhl-i-bnn-1.jpg",
        'overview_image' => "https://img.veenaworld.com/group-tours/india/leh-ladakh/lhdl-id/lhdl-id-ovw.jpg",
        'start_point' => $faker->city(),
        'end_point' => $faker->city(),
        'inclusions' => arrayToUnorderedList($faker, mt_rand(8, 12)),
        'exclusions' => arrayToUnorderedList($faker, mt_rand(4, 7)),
        'remarks' => arrayToUnorderedList($faker, mt_rand(2, 7)),
        'cancellation_terms' => arrayToUnorderedList($faker, mt_rand(2, 7)),
        'special_notes' => arrayToUnorderedList($faker, mt_rand(2, 7)),
        'tour_highlights' => arrayToUnorderedList($faker, 4)
    ];
});