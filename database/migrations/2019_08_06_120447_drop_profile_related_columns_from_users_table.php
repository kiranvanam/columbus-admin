<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropProfileRelatedColumnsFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            if(Schema::hasColumn('users', 'personal_email'))
                $table->dropColumn('personal_email');

            if(Schema::hasColumn('users', 'personal_phone'))
                $table->dropColumn('personal_phone');

            if(Schema::hasColumn('users', 'permanent_address'))
                $table->dropColumn('permanent_address');

            if(Schema::hasColumn('users', 'current_address'))
                $table->dropColumn('current_address');

            if(Schema::hasColumn('users', 'city'))
                $table->dropColumn('city');

            if(Schema::hasColumn('users', 'country'))
                $table->dropColumn('country');

            if(Schema::hasColumn('users', 'region'))
                $table->dropColumn('region');

            if(Schema::hasColumn('users', 'postal_code'))
                $table->dropColumn('postal_code');

            if(Schema::hasColumn('users', 'owner'))
                $table->dropColumn('owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
