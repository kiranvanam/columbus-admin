<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCityCountryRegionPostaCodeAddressColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('city', 50)->default('')->after('password');
            $table->string('region', 50)->default('')->after('city');
            $table->string('country', 2)->default('')->after('region');
            $table->string('address', 500)->default('')->after('country');
            $table->string('postal_code', 20)->default('')->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('city');
            $table->dropColumn('region');
            $table->dropColumn('country');
            $table->dropColumn('address');
            $table->dropColumn('postal_code');
        });
    }
}
