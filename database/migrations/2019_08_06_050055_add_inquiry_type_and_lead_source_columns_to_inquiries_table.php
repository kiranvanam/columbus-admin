<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInquiryTypeAndLeadSourceColumnsToInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inquiries', function (Blueprint $table) {
            $table->string('inquiry_type',100)->nullable()->after('name');
            $table->string('lead_source_type', 100)->nullable()->after('created_by');
            $table->string('lead_source_name', 100)->nullable()->after('lead_source_type');
            $table->string('priority', 100)->default('')->after('lead_source_name');
            $table->dateTime('reply_expected_date')->nullable()->after('priority');
            $table->dateTime('closed_on')->nullable()->after('reply_expected_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inquiries', function (Blueprint $table) {
            /* $table->dropColumn('inquiry_type');
            $table->dropColumn('lead_source_type');
            $table->dropColumn('lead_source_name');
            $table->dropColumn('priority');
            $table->dropColumn('reply_expected_date');
            $table->dropColumn('closed_on');*/
        });
    }
}
