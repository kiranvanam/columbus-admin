<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('country_code');
            $table->string('name',100);
            $table->string('slug',100);
            $table->string('code',100);
            $table->string('type',20);
            $table->text('searchable_terms')->nullable();
            $table->text('short_description')->nullable();
            $table->text('long_description')->nullable();
            $table->decimal('lng',10,6)->default(0);
            $table->decimal('lat',10,6)->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->index(['name','country_code','slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destinations');
    }
}
