<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug');
            $table->string('nationality');
            $table->char('cca2', 2);
            $table->char('cca3', 3);
            $table->json('currency');
            $table->json('calling_code');
            $table->string('region');
            $table->string('subregion');
            $table->text('searchable_terms')->nullable();
            $table->text('short_description')->nullable();
            $table->text('long_description')->nullable();
            $table->boolean('serving')->default(false);
            $table->float('lat');
            $table->float('lng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
