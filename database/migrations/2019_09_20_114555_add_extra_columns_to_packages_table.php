<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraColumnsToPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            if(!Schema::hasColumn('packages', 'operator')) {
                $table->string('operator')->default('cv')->after('title');
            }
            if(!Schema::hasColumn('packages', 'price_starts_from_label')) {
                $table->string('price_starts_from_label')->nullable()->after('price_starts_from');
            }
            if(!Schema::hasColumn('packages', 'tour_duration_format')) {
                $table->string('tour_duration_format')->default('nights-days')->after('tour_type');
            }
            if(!Schema::hasColumn('packages', 'no_of_nights')) {
                $table->unsignedInteger('no_of_nights')->default(0)->after('tour_duration_format');
            }
            if(!Schema::hasColumn('packages', 'no_of_days')) {
                $table->unsignedInteger('no_of_days')->default(1)->after('no_of_nights');
            }
            if(!Schema::hasColumn('packages', 'no_of_hours')) {
                $table->unsignedInteger('no_of_hours')->default(0)->after('no_of_days');
            }
            if(!Schema::hasColumn('packages', 'no_of_minutes')) {
                $table->unsignedInteger('no_of_minutes')->default(0)->after('no_of_hours');
            }
            if(!Schema::hasColumn('packages', 'custom_duration')) {
                $table->string('custom_duration')->nullable()->after('no_of_minutes');
            }
            if(!Schema::hasColumn('packages', 'min_age')) {
                $table->decimal('min_age')->nullable()->after('custom_duration');
            }
            if(!Schema::hasColumn('packages', 'max_age')) {
                $table->decimal('max_age')->nullable()->after('min_age');
            }
            if(!Schema::hasColumn('packages', 'min_pax')) {
                $table->unsignedInteger('min_pax')->default(1)->after('max_age');
            }
            if(!Schema::hasColumn('packages', 'max_pax')) {
                $table->unsignedInteger('max_pax')->default(6)->after('min_pax');
            }
            if(!Schema::hasColumn('packages', 'tour_starts_from')) {
                $table->string('tour_starts_from')->after('max_pax');
            }
            if(!Schema::hasColumn('packages', 'tour_ends_at')) {
                $table->string('tour_ends_at')->after('tour_starts_from');
            }
            if(!Schema::hasColumn('packages', 'day_wise_destinations')) {
                $table->json('day_wise_destinations')->nullable()->after('tour_ends_at');
            }
            if(!Schema::hasColumn('packages', 'cancellation_policy')) {
                $table->text('cancellation_policy')->nullable()->after('remarks');
            }
            if(!Schema::hasColumn('packages', 'important_notes')) {
                $table->text('important_notes')->nullable()->after('overview');
            }
            if(!Schema::hasColumn('packages', 'thumbnail_image')) {
                $table->string('thumbnail_image')->nullable()->after('overview_image');
            }
            if(!Schema::hasColumn('packages', 'group_size')) {
                $table->string('group_size')->default(0)->after('thumbnail_image');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn(['operator', 'tour_duration_format', 'no_of_nights', 'no_of_days',
                'no_of_hours', 'no_of_minutes', 'custom_duration', 'min_age', 'max_age', 'min_pax',
                'max_pax', 'tour_starts_from', 'tour_ends_at', 'day_wise_destinations',
                'cancellation_policy', 'important_notes', 'thumbnail_image', 'group_size']);
        });
    }
}
