<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('reports_to')->nullable();
            $table->string('title', 10);
            $table->string('first_name', 50);
            $table->string('last_name', 50)->nullable();
            $table->string('email', 100)->unique();
            $table->string('phone', 20)->unique();
            //$table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('personal_email');
            $table->string('personal_phone');
            $table->string('permanent_address');
            $table->string('current_address');
            $table->string('city', 50);
            $table->string('region', 50);
            $table->string('country', 2)->default('IN');
            $table->string('postal_code', 20);
            $table->boolean('owner')->default(false);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
