<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('faqs', function (Blueprint $table) {
            $table->dropColumn('categories');
            if(!Schema::hasColumn('faqs', 'faqable_id'))
                $table->unsignedInteger('faqable_id')->after('id');
            if(!Schema::hasColumn('faqs', 'faqable_type'))
                $table->string('faqable_type')->after('faqable_id');
            if(!Schema::hasColumn('faqs', 'user_id'))
                $table->unsignedInteger('user_id')->nullable()->after('faqable_type');
            if(!Schema::hasColumn('faqs', 'approved'))
                $table->boolean('approved')->default(false)->after('answer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faqs', function (Blueprint $table) {
            $table->string('categories');
            $table->dropColumn(['faqable_id', 'faqable_type', 'user_id', 'approved']);
        });
    }
}
