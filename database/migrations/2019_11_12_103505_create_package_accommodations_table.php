<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageAccommodationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_accommodations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('package_id')->index();
            $table->unsignedInteger('order')->index();
            $table->unsignedInteger('destination_id')->index();
            $table->string('city_name');
            $table->string('country_name');
            $table->string('hotel_name');
            $table->string('room_category')->nullable();
            $table->unsignedInteger('no_of_nights');
            $table->timestamps();
            $table->unique(['package_id', 'order']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_accommodations');
    }
}
