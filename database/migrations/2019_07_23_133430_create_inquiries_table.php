<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('code', 20)->unique();
            $table->date('dot')->nullable();
            $table->string('package', 20)->nullable();
            $table->unsignedInteger('status_id');
            $table->text('notes');
            $table->boolean('created_by_customer')->default(false);
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('assigned_to');
            $table->unsignedInteger('created_by');
            $table->text('closed_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiries');
    }
}
