<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('account_id')->index();
            $table->string('title', 100);
            $table->string('fname', 100);
            $table->string('lname', 100);
            $table->string('email', 50)->nullable()->unique();
            $table->string('phone', 50)->nullable()->unique();
            $table->string('alt_email', 50)->nullable();
            $table->string('alt_phone', 50)->nullable();
            $table->date('dob')->nullable()->index();
            $table->string('address', 150)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('region', 50)->nullable();
            $table->string('country', 2)->default('IN');
            $table->string('postal_code', 25)->nullable();
            $table->string('role', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
