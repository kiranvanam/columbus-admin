<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('title')->unique();
            $table->string('slug')->unique();
            $table->string('tour_code')->unique();
            $table->string('tour_type');
            $table->unsignedInteger('tour_duration')->default(1); // This column is not in use anymore
            $table->unsignedInteger('no_of_countries')->default(1);
            $table->unsignedInteger('no_of_destinations')->default(1);
            $table->string('price_starts_from_currency')->nullable();
            $table->decimal('price_starts_from')->nullable();
            $table->string('banner_image');
            $table->string('overview_image');
            $table->text('overview')->nullable();
            $table->text('inclusions')->nullable();
            $table->text('exclusions')->nullable();
            $table->text('tour_highlights')->nullable();
            $table->text('payment_policy')->nullable();
            $table->text('special_notes')->nullable();
            $table->text('remarks')->nullable();
            $table->text('other_information')->nullable();
            $table->boolean('is_active')->default(false);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
