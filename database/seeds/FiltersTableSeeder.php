<?php

use App\Filter;
use Illuminate\Database\Seeder;

class FiltersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file_path = database_path('seeds/data/filters.json');
        $filters_string = file_get_contents($file_path);
        $filters = json_decode($filters_string, true);

        $this->createFilters($filters, null);
    }

    public function createFilters($filters, $parent) {
        foreach($filters as $filter) {
            $filter_obj = Filter::firstOrNew(['name' => $filter['name']]);
            $filter_obj['slug'] = str_slug($filter['name']);
            $filter_obj['description'] = $filter['description'];
            $filter_obj['parent_id'] = $parent && $parent['id'] ? $parent['id'] : null;
            $filter_obj->save();
            if($filter['children'] && count($filter['children'])) {
                $this->createFilters($filter['children'],$filter_obj);
            }
        }
    }
}
