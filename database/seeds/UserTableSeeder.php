<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'title' => 'Mr',
                'first_name' => 'Kiran Kumar',
                'last_name' => 'Vanam',
                'email' => 'kiran@columbusvacations.in',
                'password' => 'M@nalivanams2110',
                'phone' => '9248191991',
                'user_type' => 'employee',
                'city' => 'Hyderabad',
                'region' => 'Telangana',
                'country' => 'IN',
                'address' => 'Nagole',
                'postal_code' => '500068'
            ],
            [
                'title' => 'Mr',
                'first_name' => 'Satish Kumar',
                'last_name' => 'Kasanagottu',
                'email' => 'satish@columbusvacations.in',
                'password' => 'CV@12345',
                'phone' => '9666691991',
                'user_type' => 'employee',
                'city' => 'Hyderabad',
                'region' => 'Telangana',
                'country' => 'IN',
                'address' => 'DD Colony, OU',
                'postal_code' => '500068'
            ]
        ];

        foreach($users as $user) {
            $userObj = User::firstOrNew(['email' => $user['email']]);
            $userObj['title'] = $user['title'];
            $userObj['first_name'] = $user['first_name'];
            $userObj['last_name'] = $user['last_name'];
            $userObj['email'] = $user['email'];
            $userObj['password'] = $user['password'];
            $userObj['phone'] = $user['phone'];
            $userObj['user_type'] = $user['user_type'];
            $userObj['password'] = $user['password'];
            $userObj['phone'] = $user['phone'];
            $userObj['user_type'] = $user['user_type'];
            $userObj['city'] = $user['city'];
            $userObj['region'] = $user['region'];
            $userObj['country'] = $user['country'];
            $userObj['address'] = $user['address'];
            $userObj['postal_code'] = $user['postal_code'];
            $userObj->save();
        }
    }
}
