<?php

use App\Destination;
use App\Package;
use App\PackageAccommodation;
use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file_path = database_path('seeds/data/packages.json');
        $packages_string = file_get_contents($file_path);
        $packages = json_decode($packages_string, true);
        foreach($packages as $package) {
            $package_obj = Package::firstOrNew(['title' => $package['title']]);
            $package_obj['slug'] = str_slug($package['title']);
            $package_obj['tour_code'] = $package['tour_code'];
            $package_obj['operator'] = $package['operator'];
            $package_obj['tour_duration_format'] = $package['tour_duration_format'];
            $package_obj['no_of_days'] = $package['no_of_days'];
            $package_obj['no_of_nights'] = $package['no_of_nights'];
            $package_obj['no_of_hours'] = $package['no_of_hours'];
            $package_obj['no_of_minutes'] = $package['no_of_minutes'];
            $package_obj['custom_duration'] = $package['custom_duration'];
            $package_obj['min_age'] = $package['min_age'];
            $package_obj['max_age'] = $package['max_age'];
            $package_obj['min_pax'] = $package['min_pax'];
            $package_obj['max_pax'] = $package['max_pax'];
            $package_obj['tour_starts_from'] = $package['tour_starts_from'];
            $package_obj['tour_ends_at'] = $package['tour_ends_at'];
            $package_obj['overview'] = $package['overview'];
            $package_obj['inclusions'] = $package['inclusions'];
            $package_obj['exclusions'] = $package['exclusions'];
            $package_obj['day_wise_destinations'] = $this->getDayWiseDestinations($package);
            $package_obj['banner_image'] = $package['banner_image'];
            $package_obj['overview_image'] = $package['overview_image'];
            $package_obj['thumbnail_image'] = $package['thumbnail_image'];
            $package_obj['group_size'] = $package['group_size'];
            $package_obj['is_active'] = $package['is_active'];
            $package_obj->save();
            $package_obj->updateDestinationDetails([
                'day_wise_destinations' => $package_obj['day_wise_destinations']
            ]);
            $package_obj->updatePackageItineraries($package['itineraries']);
            //$package_obj->updateAccommodationDetails($package['accommodations']);

            // Set the order of the accommodation details
            for($index=0; $index<count($package['accommodations']); $index++) {
                $package['accommodations'][$index]['order'] = $index + 1;
            }
            PackageAccommodation::where('package_id', $package_obj->id)->delete();
            $package_obj->accommodations()->createMany($package['accommodations']);
        }
        echo "Packages data is seeded" . PHP_EOL;
    }

    public function getDayWiseDestinations($package)
    {
        $dayWiseDestinations = $package['day_wise_destinations'];
        for($dayCount=0; $dayCount<count($dayWiseDestinations); $dayCount++) {
            $dayDestinations = $dayWiseDestinations[$dayCount]['destinations'];
            for($destinationCount=0; $destinationCount<count($dayDestinations); $destinationCount++) {
                $destination = Destination::where('name', $dayDestinations[$destinationCount]['city_name'])->first();
                if($destination) {
                    $dayWiseDestinations[$dayCount]['destinations'][$destinationCount]['destination_id'] = $destination->id;
                }
                $dayWiseDestinations[$dayCount]['destinations'][$destinationCount]['visit_sequence'] = $destinationCount + 1;
            }
        }
        return $dayWiseDestinations;
    }
}