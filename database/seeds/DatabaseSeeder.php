<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        //$this->call(AccountsTableSeeder::class);
        //$this->call(ContactsTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(DestinationsTableSeeder::class);
        $this->call(FiltersTableSeeder::class);
        $this->call(PackagesTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);
    }
}
