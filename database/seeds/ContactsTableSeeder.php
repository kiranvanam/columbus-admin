<?php

use App\Contact;
use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account = Contact::create([
            'title' => 'Mr',
            'account_id' => 1,
            'fname' => 'Kiran Kumar',
            'lname' => 'Vanam',
            'email' => 'kiran916.vanam@gmail.com',
            'phone' => '9248191991',
            'alt_email' => 'kiran916.vanam@gmail.com',
            'alt_phone' => '9248191991',
            'city' => 'Hyderabad',
            'region' => 'Telangana',
            'country' => 'IN',
            'address' => 'Nagole',
            'postal_code' => '500068',
            'role' => 'Managing Director'
        ]);
    }
}
