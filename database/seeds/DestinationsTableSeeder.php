<?php

use App\Destination;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DestinationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $files = Storage::disk('data')->allFiles('destinations');
        foreach($files as $file) {
            // Read contents from the json file
            $destinations_json = file_get_contents(database_path('seeds/data/').$file);
            $this->createDestinations($destinations_json);
        }
        echo "Destinations data is seeded" . PHP_EOL;
    }

    public function createDestinations($destinations_json)
    {
        // Decode the json content
        $destinations = json_decode($destinations_json, true);

        if(!$destinations) {
            $destinations = [];
        }

        foreach($destinations as $destination_obj) {
            $destination = Destination::firstOrNew(['code' => $destination_obj['code']]);
            $destination['name'] = $destination_obj['name'];
            $destination['slug'] = str_slug($destination['name']);
            $destination['country_code'] = $destination_obj['country_code'];
            $destination['type'] = $destination_obj['type'];
            $destination['searchable_terms'] = $destination_obj['searchable_terms'];
            $destination['short_description'] = $destination_obj['short_description'];
            $destination['long_description'] = $destination_obj['long_description'];
            $destination->save();
        }
    }
}
