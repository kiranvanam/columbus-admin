<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries_path = database_path('seeds/data/countries.json');
        $countries_string = file_get_contents($countries_path);
        $countries = json_decode($countries_string, true);
        if(!$countries) {
            $countries = [];
        }

        foreach ($countries as $country_obj) {
            $country = Country::firstOrNew(['name' => $country_obj['name']]);
            $country['name'] = $country_obj['name']['common'];
            $country['searchable_terms'] = $country_obj['name']['common'];
            $country['slug'] = str_slug($country['name']);
            $country['cca2'] = $country_obj['cca2'];
            $country['cca3'] = $country_obj['cca3'];
            $country['nationality'] = $country_obj['demonym'];
            $country['currency'] = $this->updateCurrency($country_obj);
            $country['calling_code'] = json_encode($country_obj['callingCode']);
            $country['region'] = $country_obj['region'];
            $country['subregion'] = $country_obj['subregion'];
            if(sizeof($country_obj['latlng']) == 2) {
                $country['lat'] = $country_obj['latlng'][0];
                $country['lng'] = $country_obj['latlng'][1];
            } else {
                $country['lat'] = 0;
                $country['lng'] = 0;
            }
            $country->save();
        }

        $servingCountries = ['AE', 'AQ', 'AU', 'AT', 'BE', 'BG', 'BH', 'BS', 'BA', 'BY', 'BM',
                'BR', 'BB', 'BT', 'BW', 'CA', 'CH', 'CL', 'CN', 'CZ', 'DE', 'DK', 'EG', 'ES',
                'EE', 'FI', 'FJ', 'FR', 'GB', 'GE', 'GR', 'GL', 'HK', 'HR', 'HU', 'ID', 'IN',
                'IE', 'IS', 'IL', 'IT', 'JO', 'JP', 'KE', 'KH', 'KR', 'LI', 'LK', 'LS', 'LT',
                'LU', 'LV', 'MO', 'MA', 'MC', 'MD', 'MG', 'MV', 'MX', 'MK', 'MT', 'ME', 'MN',
                'MU', 'MY', 'NA', 'NL', 'NO', 'NP', 'NZ', 'OM', 'PE', 'PH', 'PL', 'PT', 'RE',
                'RO', 'RU', 'RW', 'SG', 'SJ', 'RS', 'SK', 'SI', 'SE', 'SZ', 'SC', 'TH', 'TR',
                'TW', 'TZ', 'US', 'VA', 'VE', 'VN', 'VU', 'ZA', 'ZM'];

        Country::whereIn('cca2', $servingCountries)->update(['serving' => true]);

        echo "Countries data is seeded" . PHP_EOL;
    }

    protected function updateCurrency($country) {
        switch(strtoupper($country['cca2'])) {
            case 'BO': return ['BOB'];
            case 'BT': return ['BTN'];
            case 'CH': return ['CHF'];
            case 'CL': return ['CLP'];
            case 'CU': return $country['currency'];
            case 'EH': return $country['currency'];
            case 'HT': return $country['currency'];
            case 'LS': return $country['currency'];
            case 'NA': return $country['currency'];
            case 'PA': return $country['currency'];
            case 'SV': return $country['currency'];
            case 'UY': return $country['currency'];
            case 'US': return ['USD'];

            default: return $country['currency'];
        }
    }
}