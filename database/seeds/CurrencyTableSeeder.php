<?php

use App\Currency;
use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file_path = database_path('seeds/data/currencies.json');
        $currencies_string = file_get_contents($file_path);
        $currencies = json_decode($currencies_string, true);

        $this->createCurrencies($currencies, null);
    }

    public function createCurrencies($currencies) {
        foreach($currencies as $currency) {
            $currency_obj = Currency::firstOrNew(['symbol' => $currency['symbol']]);
            $currency_obj['name'] = $currency['name'];
            $currency_obj['code'] = $currency['code'];
            $currency_obj['is_active'] = true;
            $currency_obj->save();
        }
    }
}