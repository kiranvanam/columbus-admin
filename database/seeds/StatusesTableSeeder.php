<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'name' => 'Quote Pending',
                'type' => 'inquiry'
            ],
            [
                "name" => "Waiting For Vendor Quotation",
                "type" => "inquiry"
            ],
            [
                'name' => 'Quoted - Awaiting Revert',
                'type' => 'inquiry'
            ],
            [
                'name' => 'Hold',
                'type' => 'inquiry'
            ],
            [
                'name' => 'Postponed',
                'type' => 'inquiry'
            ],
            [
                'name' => 'Booked',
                'type' => 'inquiry'
            ],
            [
                'name' => 'Lead Lost',
                'type' => 'inquiry'
            ],
            [
                'name' => 'Cancelled Program',
                'type' => 'inquiry'
            ]
        ];
        foreach($statuses as $status) {
            $status['slug'] = str_slug($status['name']);
            $obj = Status::firstOrCreate($status);
            $obj->update($status);
        }
    }
}