<?php

use App\Account;
use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account = Account::create([
            'name' => 'Vanam Household',
            'email' => 'kiran916.vanam@gmail.com',
            'phone' => '9248191991',
            'city' => 'Hyderabad',
            'region' => 'Telangana',
            'country' => 'IN',
            'address' => 'Nagole',
            'postal_code' => '500068',
            'type' => 'family',
        ]);
    }
}
