<?php

Route::group(['prefix' => 'website', 'middleware' => ['web']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/collections', 'PackagesListController@packages');
    Route::get('/c/{country}', 'PackagesListController@countryPackages');
    Route::get('/tour-package/{package}', 'PackagesController@index');
});