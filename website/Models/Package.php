<?php

namespace Website\Models;

use App\Package as BasePackage;

class Package extends BasePackage
{
    public function resolveRouteBinding($value) {
        return $this->where('slug', $value)->with('destinations')->with('itineraries')->first();
    }
}