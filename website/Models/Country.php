<?php

namespace Website\Models;

use App\Country as BaseCountry;

class Country extends BaseCountry
{
    public function getRouteKeyName() {
        return 'cca2';
    }

    public function resolveRouteBinding($value) {
        return $this->where('cca2', $value)->first();
    }
}