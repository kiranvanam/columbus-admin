<?php

if(! function_exists('website_database_path')) {
    function website_database_path ($path = '') {
        $path = trim($path, '/');
        return base_path('website/database/'.$path);
    }
}

if(! function_exists('is_the_last_item_in_the_collection')) {
    function is_the_last_item_in_the_collection ($collection, $loopIteration) {
        if($collection && ($collection->count() == $loopIteration)) {
            return true;
        }
        return false;
    }
}

if(! function_exists('is_the_last_item_in_the_array')) {
    function is_the_last_item_in_the_array ($arr, $loopIteration) {
        if($arr && (count($arr) == $loopIteration)) {
            return true;
        }
        return false;
    }
}

if(! function_exists('stringToArray')) {
    function stringToArray($char, $str) {
        return array_filter(explode($char, $str), function ($item) {
            if(empty(trim($item))) {
                return false;
            }
            return true;
        });
    }
}

if(! function_exists('getIconIdentifier')) {
    function getIconIdentifier($type) {
        switch (strtolower($type)) {
            case 'breakfast':
                return 'icon-cutlery';
            case 'lunch':
                return 'icon-meal';
            case 'dinner':
                return 'icon-meal';
            case 'tea':
                return 'icon-coffee';
            case 'coffee':
                return 'icon-coffee';
            case 'cream':
                return 'icon-icecream';
            case 'ice-cream':
                return 'icon-icecream';
            case 'snacks':
                return 'icon-bread-slice';
        }
    }
}