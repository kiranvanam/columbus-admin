<?php

namespace Website\Http\Controllers;

use App\Country;
use App\Destination;
use Website\Models\Package;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;


class PackagesController extends Controller
{
    public function index(Package $package) {
        $menu = $this->getMenuData();
        return view('website::packages', [
            'menu' => $menu,
            'package' => $package
        ]);
    }

    protected function getPackageData() {
        $package_path = website_database_path('/seeds/package-data.json');
        $package_string = file_get_contents($package_path);
        return json_decode($package_string);
    }
}