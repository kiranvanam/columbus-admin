<?php

namespace Website\Http\Controllers;

use Website\Models\Country;
use App\Destination;
use Website\Models\Package;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;


class PackagesListController extends Controller
{
    public function countryPackages(Country $country) {
        $menu = $this->getMenuData();
        $country->load('destinations.packages');

        $packages = $country->destinations->pluck('packages')->flatten()->unique('id');

        return view('website::collections', [
            'menu' => $menu,
            'title' => $country->name,
            'short_description' => $country->short_description,
            'long_description' => $country->long_description,
            'search_title' => $country->name,
            'packages' => $packages
        ]);
    }

    public function packages() {
        $menu = $this->getMenuData();

        $packages = Package::all();
        $country = Country::where('cca2', 'IN')->first();

        return view('website::collections', [
            'menu' => $menu,
            'title' => $country->name,
            'short_description' => $country->short_description,
            'long_description' => $country->long_description,
            'search_title' => $country->name,
            'packages' => $packages
        ]);
    }

    protected function getPackageData() {
        $package_path = website_database_path('/seeds/package-data.json');
        $package_string = file_get_contents($package_path);
        return json_decode($package_string);
    }

    protected function getPackageData2() {
        return [];
    }
}