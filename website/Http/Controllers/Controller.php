<?php

namespace Website\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    protected function getMenuData() {
        $menu_path = website_database_path('seeds/menu.json');
        return json_decode(file_get_contents($menu_path));
    }
}