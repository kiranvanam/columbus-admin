<!doctype html>
<html class="tw-font-sans tw-bg-white tw-antialiased" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        
        <link href="{{ mix('/css/website/app.css') }}" rel="stylesheet">
        <script src="{{ mix('/js/website/app.js') }}" ></script>
    </head>
    <body class="tw-min-h-screen tw-bg-white tw-tracking-wide tw-leading-normal">
        @component('website::components.svg-defs')
        @endcomponent
        <div class="tw-flex tw-flex-col tw-min-h-screen">
            <!-- Header Section -->

            @component('website::components.header', ['menu' => $menu])
            @endcomponent

            <!-- Main Content Section -->
            @yield('content')

            <!-- Footer Component -->
            @component('website::components.footer')
            @endcomponent
        </div>
        <script>
            $(document).ready(function(){
                $('.owl-main').owlCarousel({
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause: true,
                    margin: 20,
                    center: true,
                    loop: true,
                    items: 1.2,
                    nav: true,
                    dots: false,
                    navText : [
                        "<span class='group'><img class='group-hover:tw-hidden' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block' src='/images/arrowInCircleYellow.svg'></span>",
                        "<span class='group'><img class='group-hover:tw-hidden rotate-180' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block rotate-180' src='/images/arrowInCircleYellow.svg'></span>",
                    ]
                });

                $('.owl-reviews').owlCarousel({
                    autoplay:true,
                    margin: 20,
                    autoplayTimeout:3000,
                    autoplayHoverPause: true,
                    loop: true,
                    nav: true,
                    dots: false,
                    navText : [
                        "<span class='group'><img class='group-hover:tw-hidden' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block' src='/images/arrowInCircleYellow.svg'></span>",
                        "<span class='group'><img class='group-hover:tw-hidden rotate-180' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block rotate-180' src='/images/arrowInCircleYellow.svg'></span>",
                    ],
                    responsive: {
                        0: {
                            items: 1
                        },
                        576: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        1000: {
                            items: 4
                        }
                    }
                });
                
                $('.owl-inspiration').owlCarousel({
                    autoplay:true,
                    margin: 20,
                    autoplayTimeout:3000,
                    autoplayHoverPause: true,
                    loop: true,
                    nav: true,
                    dots: false,
                    navText : [
                        "<span class='group'><img class='group-hover:tw-hidden' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block' src='/images/arrowInCircleYellow.svg'></span>",
                        "<span class='group'><img class='group-hover:tw-hidden rotate-180' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block rotate-180' src='/images/arrowInCircleYellow.svg'></span>",
                    ],
                    responsive: {
                        0: {
                            items: 1
                        },
                        576: {
                            items: 2
                        },
                        768: {
                            items: 3
                        }
                    }
                });

                $('.owl-travel-style').owlCarousel({
                    autoplay:true,
                    margin: 20,
                    autoplayTimeout:3000,
                    autoplayHoverPause: true,
                    loop: true,
                    nav: true,
                    dots: false,
                    navText : [
                        "<span class='group'><img class='group-hover:tw-hidden' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block' src='/images/arrowInCircleYellow.svg'></span>",
                        "<span class='group'><img class='group-hover:tw-hidden rotate-180' src='/images/arrowInCircleGrey.svg'><img class='tw-hidden group-hover:tw-block rotate-180' src='/images/arrowInCircleYellow.svg'></span>",
                    ],
                    responsive: {
                        0: {
                            items: 1
                        },
                        576: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        1345: {
                            items: 4
                        }
                    }
                });
            });

            
        </script>
        <script src="/js/website/noframework.waypoints.min.js"></script>
        <script src="/js/website/inview.min.js"></script>
        <script>
            // Just add this code after Waypoints import/inclusion
            Waypoint.Inview.prototype.createWaypoints = function() {
                // same as jQuery.outerHeight() function, works for IE9+
                function outerHeight(el) {
                    var height = el.offsetHeight;
                    var style = getComputedStyle(el);

                    height += parseInt(style.marginTop) + parseInt(style.marginBottom);
                    return height;
                }

                var configs = {
                    vertical: [
                        {
                            down: 'enter',
                            up: 'exited',
                            offset: function() {
                                var _offset = this.options.offset && this.options.offset.bottom || 0;
                                return this.options.context.innerHeight - _offset;
                            }.bind( this )
                        },
                        {
                            down: 'entered',
                            up: 'exit',
                            offset: function() {
                                var _offset = this.options.offset && this.options.offset.bottom || 0;
                                return this.options.context.innerHeight - outerHeight(this.element) - _offset;
                            }.bind( this )
                        },
                        {
                            down: 'exit',
                            up: 'entered',
                            offset: function() {
                                var _offset = this.options.offset && this.options.offset.top || 0;
                                return _offset;
                            }.bind( this )
                        },
                        {
                            down: 'exited',
                            up: 'enter',
                            offset: function() {
                                var _offset = this.options.offset && this.options.offset.top || 0;
                                return _offset - outerHeight(this.element);
                            }.bind( this )
                        }
                    ],
                    horizontal: [
                        {
                            right: 'enter',
                            left: 'exited',
                            offset: '100%'
                        },
                        {
                            right: 'entered',
                            left: 'exit',
                            offset: 'right-in-view'
                        },
                        {
                            right: 'exit',
                            left: 'entered',
                            offset: 0
                        },
                        {
                            right: 'exited',
                            left: 'enter',
                            offset: function() {
                                return -this.adapter.outerWidth()
                            }
                        }
                    ]
                };

                for (var i = 0, end = configs[this.axis].length; i < end; i++) {
                    var config = configs[this.axis][i]
                    this.createWaypoint(config)
                }
            };
        </script>
        <script>
            (function() {
                let navItems = [
                    'overview', 'itinerary', 'information', 'faq', 'dates-prices'
                ];
                navItems.forEach(function(navItem){ createWayPoints(navItem)});
            })();
            function toggleNavItemsClass(navItem) {
                $("#package-navigation-list a").removeClass('active');
                $("#nav-"+navItem).addClass('active');
            }
            function createWayPoints(navItem) {
                var element = document.getElementById(""+navItem);
                let prepareConfig = {
                    element: element,
                    enter: function(direction) {
                        if(direction === 'up') {
                            toggleNavItemsClass(navItem);
                        }
                    },
                    exit: function(direction) {
                        if(direction === 'down') {
                            toggleNavItemsClass(navItem);
                        }
                    },
                    offset: {
                        top: 75
                    }
                };
                new Waypoint.Inview(prepareConfig);
            }
        </script>
    </body>
</html>