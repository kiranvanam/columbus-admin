@extends('website::layouts.app')

@section('content')

<!-- Main Section -->
<main class="tw-flex-grow">
    @component('website::components.collections.description', [
        'title' => $title,
        'short_description' => $short_description,
        'long_description' => $long_description
    ])
    @endcomponent

    @component('website::components.collections.main', [
        'search_title' => $search_title,
        'packages' => $packages
    ])
    @endcomponent
</main>

<!-- End Of Main Section -->

@endsection