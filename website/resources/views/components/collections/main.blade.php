<section class="tw-mb-8 tw-px-8">
    <div>
        <h2 class="tw-text-xl tw-font-bold">{{ $search_title }} Holiday Packages</h2>
        <div class="tw-text-right">
            <span class="tw-text-sm">Showing 1 - 10 of 100 results</span>
        </div>
    </div>
    <div class="tw-flex tw-flex-nowrap lg:tw--mr-8">
        <aside class="tw-hidden lg:tw-block lg:tw-w-1/4 lg:tw-pr-4">
            @component('website::components.collections.filter')
            @endcomponent
        </aside>
        <div class="tw-w-full lg:tw-w-3/4 lg:tw-pr-8">
            @component('website::components.collections.search-results', [
                'search_title' => $search_title,
                'packages' => $packages
            ])
            @endcomponent
        </div>
    </div>
</section>
{{--
@component('website::components.collections.information', ['collectionInformation' => $collection->information])
@endcomponent
--}}