<div class="tw-flex tw-justify-between tw-items-center">
    <div>
        <button class="tw-cursor-pointer">
            <span class="tw-text-sm">Sort By</span>
            <span class="tw-text-sm tw-font-bold tw-ml-1">Price Low To High</span>
        </button>
        <span class="inline-block tw-ml-4 lg:tw-hidden">
            <svg class="icon icon-filter tw-text-4xl tw-text-primary-600"><use xlink:href="#icon-filter"></use></svg>
        </span>
    </div>
</div>
<div class="tw-mt-4">
    <div class="tw-flex tw-items-stretch tw-flex-wrap" style="margin:-10px;">
        @foreach($packages as $package)
            @component('website::components.collections.search-item', ['package' => $package])
            @endcomponent
        @endforeach
    </div>
</div>