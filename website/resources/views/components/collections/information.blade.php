<article class="tw-bg-gray-100">
    <div class="container tw-py-20 tw-text-sm tw-leading-loose tw-text-justify">
        <p>
            {{ $collectionInformation }}
        </p>
    </div>
</article>