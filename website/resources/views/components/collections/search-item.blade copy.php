<div class="tw-w-full sm:tw-w-1/2 xl:tw-w-1/3" style="padding:10px">
    <div class="card tw-relative tw-flex tw-flex-col tw-break-words tw-shadow">
        <div class="card-image-wrapper tw-relative">
            <a href="" class="tw-block tw-w-full tw-cursor-pointer">
                <img src="{{$package->thumbnail_image}}" alt="{{$package->title}}">
            </a>
            <a class="tw-absolute tw-bottom-0 tw-w-full gradient tw-cursor-pointer" style="height: 56%"></a>
            <a class="tw-absolute tw-text-white tw-bottom-0 tw-w-full tw-p-4 tw-cursor-pointer">
                <span class="tw-text-xs tw-font-extrabold tw-uppercase">
                    <small>{{$package->tour_type}} - {{$package->code}}</small>
                </span>
                <h2 class="tw-font-extrabold">
                    {{$package->title}} <br> 
                    <span class="tw-text-xs">
                        @if($package->tour_duration_format === 'nights-days')
                            {{$package->no_of_nights}}
                            {{($package->no_of_nights > 1) ? 'Nights' : 'Night'}} / 
                            {{$package->no_of_days}}
                            {{($package->no_of_days > 1) ? 'Days' : 'Day'}}
                        @endif
                    </span>
                </h2>
            </a>
            <div class="tw-absolute tw-top-0 tw-left-0 tw-px-1 tw-rounded tw-ml-2 tw-mt-2 tw-bg-indigo-600 tw-text-xs tw-text-white">
                {{$package->getTourTypeName()}}
            </div>
            <div class="tw-absolute tw-top-0 tw-right-0 tw-px-1 tw-rounded tw-ml-2 tw-mt-2 tw-text-xs tw-text-white">
                @if($package->isGold())
                    <img style="width:75px;" src="/images/gold.png" alt="Gold Package">
                @elseif($package->isPlatinum())
                    <img style="width:90px;" src="/images/platinum.png" alt="Platinum Package">
                @endif
            </div>
        </div>
        <div class="card-content tw-p-4">
            <a href="" class="tw-block tw-cursor-pointer">
                <ul class="tw-flex tw-justify-between tw-flex-wrap">
                    <li class="tw-inline-block">
                        <span class="tw-text-xs">
                            <small class="tw-uppercase">STARTS FROM</small>
                        </span>
                        <div>
                            <h3 class="tw-font-extrabold tw-text-center">{!! $package->getCurrencyCode() !!} {{round($package->price_starts_from)}}</h3>
                        </div>
                    </li>
                    <li class="tw-inline-block">
                        <span class="tw-text-xs">
                            <small class="tw-uppercase">COUNTRIES</small>
                        </span>
                        <div>
                            <h3 class="tw-font-extrabold tw-text-center">
                                {{$package->no_of_countries}}
                            </h3>
                        </div>
                    </li>
                    <li class="tw-inline-block">
                        <span class="tw-text-xs">
                            <small class="tw-uppercase">Cities</small>
                        </span>
                        <div>
                            <h3 class="tw-font-extrabold tw-text-center">{{$package->no_of_destinations}}</h3>
                        </div>
                    </li>
                </ul>
            </a>
            
            <div class="tw-flex tw-font-extrabold tw-justify-between tw-mt-4 tw-text-center tw-text-xxs">
                <a href="" class="tw-bg-primary-600 tw-cursor-pointer tw-inline-block tw-px-2 tw-py-2 tw-text-white tw-uppercase">
                    View Tour Details
                </a>
                <a href="" class="hover:tw-bg-primary-600 hover:tw-text-white tw-border tw-border-primary-600 tw-cursor-pointer tw-inline-block tw-px-2 tw-py-2 tw-uppercase">
                    Download PDF
                </a>
                
            </div>
            <!--
            <div class="tw-flex tw-text-xxs tw-mt-4 tw-flex-col tw-items-end tw-text-center tw-font-extrabold">
                <a href="" class="tw-cursor-pointer tw-border tw-w-2/3 hover:tw-bg-primary-600 hover:tw-text-white tw-border-primary-600 tw-uppercase tw-inline-block tw-px-4 tw-py-2">
                    Download PDF
                </a>
                <a href="" class="tw-cursor-pointer tw-mt-2 tw-text-white tw-w-2/3 tw-bg-primary-600 tw-uppercase tw-inline-block tw-px-4 tw-py-2">
                    View Tour Details
                </a>
            </div>
            -->
            <div class="tw-flex tw-flex-col tw-mt-4">
                <a class="tw-cursor-pointer tw-text-right">
                    <span class="tw-text-gray-500 hover:tw-text-primary-600 tw-font-medium tw-text-sm">
                        Quick View
                        <svg class="icon icon-view-show"><use xlink:href="#icon-view-show"></use></svg>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>