<section class="tw-mb-8 tw-px-8">
    <div class="tw-flex tw-flex-row tw-flex-wrap tw-py-8 tw-border-b tw-border-gray-400">
        <div class="tw-w-full md:tw-w-1/4">
            <div class="tw-w-11/12">
                <h2 class="tw-text-4xl tw-font-extrabold">{{ $title }} Tour Packages</h2>
                <p class="tw-mt-2 tw-leading-loose">{{ $short_description }}</p>
            </div>
        </div>
        <div class="tw-w-full md:tw-w-3/4">
            <p class="tw-mt-2 tw-text-sm tw-text-justify">{{ $long_description }}</p>
        </div>
    </div>
</section>