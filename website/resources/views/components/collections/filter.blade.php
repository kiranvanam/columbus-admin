<div class="tw-px-4 tw-bg-gray-200">
    <div class="tw-flex tw-flex-nowrap tw-justify-between tw-items-center tw-leading-loose">
        <div>
            <span class="tw-text-sm">Filter your search</span>
        </div>
        <div class="tw-text-xs tw-cursor-pointer tw-text-teal-600 hover:tw-underline">
            Reset all
        </div>
    </div>
    <div class="tw-py-4">
        <h2 class="tw-text-sm tw-font-extrabold tw-my-2">Package Type</h2>
        <div>
            <input type="checkbox" name="package_type" id=""> <span class="tw-text-sm">Group Tour (Incl. Flights)</span>
        </div>
        <div>
            <input type="checkbox" name="package_type" id=""> <span class="tw-text-sm">Group Tour (Excl. Flights)</span>
        </div>
        <div>
            <input type="checkbox" name="package_type" id=""> <span class="tw-text-sm">Customized Holiday</span>
        </div>
    </div>
    <div class="tw-py-4">
        <h2 class="tw-text-sm tw-font-extrabold">Departure Dates Between</h2>
        <div class="tw-bg-gray-200 tw-p-2 tw-mt-4">
            <table class="tw-w-full tw-border-collapse tw-table-fixed tw-text-sm">
                <tr>
                    <td class="tw-text-bold tw-text-center tw-cursor-pointer">Start Date</td>
                    <td class="tw-text-bold tw-text-center"><svg class="icon icon-arrow tw-text-xl"><use xlink:href="#icon-arrow"></use></svg></td>
                    <td class="tw-text-bold tw-text-center tw-cursor-pointer">End Date</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="tw-py-4">
        <h2 class="tw-text-sm tw-font-extrabold">Duration</h2>
    </div>
</div>
