<section class="tw-w-full tw-mb-16">
    <div class="container">
        <h2 class="tw-font-light tw-text-7xl tw-mb-6">What our customers say</h2>
        <div class="owl-reviews owl-container-carousel owl-carousel owl-theme">
            @for($i=0; $i<10; $i++)
            <div class="tw-overflow-hidden tw-shadow-lg tw-rounded tw-flex tw-flex-col tw-px-2 tw-py-4 tw-border">
                <div class="tw-mx-auto tw-mb-4">
                    <img style="width:6rem!important;" class="tw-block sm:tw-flex-shrink-0 tw-h-auto tw-rounded-full" src="https://randomuser.me/api/portraits/women/17.jpg" alt="">
                </div>
                <div class="tw-text-center tw-text-sm  tw-mb-4">
                    The Scandinavia tour was well conducted and explored the interior parts of Scandinavia that
                    <div>
                        <a href="" class="tw-text-blue-400">Read More</a>
                    </div>
                </div>
                <div class="tw-text-center tw-uppercase">
                    SANJUKTA D
                </div>
                <div class="tw-text-center tw-text-xs">
                    <span>Visited Europe in June 2018</span>
                </div>
            </div>
            @endfor
        </div>
    </div>
</section>