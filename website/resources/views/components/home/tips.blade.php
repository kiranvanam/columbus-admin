<section class="tw-bg-gray-100 tw-flex tw-justify-center tw-mb-16">
    <div class="container tw-py-4">
        <div class="tw-flex tw-flex-col md:tw-flex-row md:tw-items-center md:tw-flex-between">
            <div class="tw-hidden md:tw-block md:tw-w-3/12 lg:tw-w-2/12">
                <img src="https://www.cosmosvacations.in/wp-content/uploads/2016/08/trip-logo.png" alt="Columbus Vacations">
            </div>
            <div class="tw-w-full tw-mb-10 md:tw-mb-0 md:tw-px-4 md:tw-w-6/12">
                <h2 class="tw-font-light tw-text-2xl lg:tw-mb-2">Want Insider Travel Tips?</h2>
                <p class="tw-text-gray-700 tw-text-sm">As you plan – or just dream about – your next vacation, we invite you to get inspired by the Columbus<br class="lg:tw-hidden"> travel blog showcasing news, tips, stories and more!</p>
            </div>
            <div class="tw-w-full md:tw-px-4 md:tw-text-center md:tw-w-3/12 lg:tw-w-4/12">
                <a target="_blank" class="tw-inline-block tw-px-6 tw-py-2 tw-bg-primary-600 tw-text-white" href="">See the Columbus Travel Blog</a>
            </div>
        </div>
    </div>
</section>