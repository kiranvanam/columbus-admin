<section class="tw-w-full tw-mb-16">
    <div class="container">
        <h2 class="tw-font-light tw-text-7xl tw-mb-6">What's Your Travel Style?</h2>
        <div class="owl-travel-style owl-container-carousel owl-carousel owl-theme">
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 140%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="https://img.veenaworld.com/home/collections/jubilee-special-18042019.jpg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Family</span>
                    </div>
                </figure>
            </a>
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 140%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="https://img.veenaworld.com/home/offers/honeymoon-thailand-08082019.jpg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Honeymoon</span>
                    </div>
                </figure>
            </a>
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 140%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="https://img.veenaworld.com/home/collections/himalayas-08022019.jpg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Seniors</span>
                    </div>
                </figure>
            </a>
            <a target="_blank" href="www.columbusvacations.in" class="tw-block">
                <figure class="tw-relative tw-m-0 tw-overflow-hidden tw-block" style="padding-bottom: 140%">
                    <img class="tw-absolute tw-h-full tw-w-full tw-object-cover" src="https://img.veenaworld.com/home/collections/eastern-europe-08022019.jpg" alt="">
                    <div class="tw-absolute tw-bottom-0 tw-w-full gradient" style="height:70%"></div>
                    <div class="tw-absolute tw-bottom-0 tw-w-full tw-text-white tw-p-6">
                        <span class="tw-font-bold tw-text-3xl">Solo</span>
                    </div>
                </figure>
            </a>
        </div>
    </div>
</section>