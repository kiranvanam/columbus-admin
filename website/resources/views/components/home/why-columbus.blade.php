
<section class="tw-w-full tw-mb-16">
    <div class="container">
        <h2 class="tw-font-light tw-text-7xl tw-mb-8 tw-text-center">Why Columbus?</h2>
        <div class="tw-flex tw-flex-wrap md:tw--mx-4 tw--my-6">
            @foreach($whyColumbus as $whyItem)
            <div class="tw-flex tw-flex-col tw-items-center tw-px-4 tw-py-6 tw-w-full md:tw-w-1/2 lg:tw-w-1/3">
                <div class="tw-bg-gray-700 tw-flex tw-justify-center tw-items-center tw-text-white tw-mb-4" style="width:60px; height: 60px;">
                    <svg class="icon icon-{{$whyItem['icon']}} tw-text-7xl"><use xlink:href="#icon-{{$whyItem['icon']}}"></use></svg>
                </div>
                <h4 class="tw-uppercase tw-mb-4"> {{$whyItem['title']}} </h4>
                <p class="tw-text-center">Spectacular train rides, breathtaking cable car ascents, overnight cruise ships, scenic day ferries, private first-class motorcoaches (most with free Wi-Fi) - Columbus offers a wonderful variety of transportation for a memorable travel experience.</p>
            </div>
            @endforeach
        </div>
    </div>
</section>