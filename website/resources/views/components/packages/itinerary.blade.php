@foreach($package->itineraries as $day)
<div id="itinerarary-day-{{$loop->iteration}}" class="tw-flex tw-flex-col tw-pb-8 scroll-margin-top-16 @if($package->itineraries && ($package->itineraries->count() !== $loop->iteration)){{'tw-border-b tw-mb-8'}}@endif">
    <span class="tw-text-sm tw-mb-1 tw-uppercase tw-block">Day {{ $loop->iteration }}</span>
    <h3 class="tw-text-4xl tw-font-extrabold">{{$day->title}}</h3>
    <div class="tw-flex tw-flex-nowrap">
        <div class="tw-w-full">
            @php
                $highlightsTags = stringToArray(",", $day->day_highlights_tags);
            @endphp
            @if(count($highlightsTags))
            <div class="tw-mt-4">
                @foreach($highlightsTags as $tag)
                    <span class="tw-mr-1 tw-mt-1 tw-inline-block tw-px-2 tw-leading-loose tw-rounded-lg tw-bg-gray-200 tw-py-1 tw-text-sm">{{$tag}}</span>
                @endforeach
            </div>
            @endif

            <p class="tw-mt-4 tw-leading-loose tw-text-justify">{!! $day->description !!}</p>

            @if(count($day->additional_details))
                <div class="tw-text-sm">
                    @foreach($day->additional_details as $additionalDetail)
                        <div class="list-info tw-mt-4">
                            <h4 class="tw-font-bold">{{$additionalDetail['title']}}:</h4>
                            <p>{!! $additionalDetail['description'] !!}</p>
                        </div>
                    @endforeach
                </div>
            @endif

            @if(count($day->meals))
                <div class="tw-mt-4">
                    @foreach($day->meals as $key => $meal)
                        <div class="tw-mr-4 tw-font-bold tw-text-sm tw-inline-flex tw-items-center">
                            <svg class="icon {{getIconIdentifier($meal['value'])}} tw-mr-2"><use xlink:href="#{{getIconIdentifier($meal['value'])}}"></use></svg>
                            <span>{{$meal['label']}}</span>
                        </div>
                    @endforeach
                </div>
            @endif

            @if(!is_the_last_item_in_the_collection($package->itineraries, $loop->iteration))
                <div class="tw-font-bold tw-mt-4">
                    <svg class="icon icon-bedroom"><use xlink:href="#icon-bedroom"></use></svg>
                    <span class="tw-text-sm">{{$day->overnight_stay}}</span>
                </div>
            @endif
        </div>
    </div>
</div>
@endforeach