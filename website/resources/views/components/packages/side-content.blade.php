<div class="tw-flex tw-flex-col">
    <div class="tw-bg-primary-600 tw-text-white">
        <div class="tw-px-4 tw-py-2">
            <span class="tw-font-extrabold">Destinations You Will Visit</span>
        </div>
    </div>
    <div class="tw-px-4 tw-bg-gray-200">
        @foreach ($package->day_wise_destinations as $day) 
        <div class="tw-text-sm @if(!$loop->last) {{'tw-border-gray-500 tw-border-b'}} @endif">
            <a href="#itinerarary-day-{{$loop->iteration}}" class="tw-flex tw-flex-nowrap tw-justify-between tw-items-center">
                <div>
                    @foreach ($day['destinations'] as $destination)
                    <div class="tw-py-2">
                        <span>{{$destination['city_name']}}</span>
                        <span style="color:#929292;"> - {{$destination['country_name']}}</span>
                        @if($loop->last && !$loop->parent->last)
                            <svg class="icon icon-bedroom"><use xlink:href="#icon-bedroom"></use></svg>
                        @endif
                    </div>
                    @endforeach
                </div>
                <div>
                    <span>Day {{$day['day_number']}}</span>
                </div>
            </a>
        </div>
        @endforeach
    </div>
    <div class="tw-bg-gray-400 tw-text-xs">
        <div class="tw-p-2">
            <span class="tw-font-bold">Note:</span> Under unavoidable circumstances itineraries may be changed or reversed
        </div>
    </div>
</div>