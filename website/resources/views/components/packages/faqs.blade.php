<section class="tw-mt-16 scroll-margin-top-16" id="faq">
    <div class="container list-info">
        <div class="tw-mb-8"><h2 class="tw-text-8xl tw-font-thin">FAQs</h2></div>
        <ol class="tw-w-full lg:tw-w-2/3">
            @foreach($package->faqs as $faq)
                <li class="tw-mb-4">
                    <h3 class="tw-font-bold">{{$faq->question}}</h3>
                    <p>{!! $faq->answer !!}</p>
                </li>
            @endforeach
        </ol>
    </div>
</section>