<section>
    <div class="tw-relative">
        <div>
            <img src="{{$package->banner_image}}" alt="{{$package->title}}">
        </div>
        <div class="tw-absolute tw-inset-0 tw-w-full gradient"></div>
        <div class="tw-absolute tw-bottom-0 tw-height-auto tw-w-full tw-p-4 md:tw-p-8">
            <div class="tw-flex tw-flex-wrap tw-items-center tw-justify-between tw-text-white">
                <div class="tw-w-full md:tw-w-1/2">
                    <span class="tw-text-sm tw-font-bold">{{$package->getTourTypeName()}} - {{$package->tour_code}}</span>
                    <span class="tw-font-extrabold tw-text-7xl tw-block">{{$package->title}}</span>
                </div>
                <div class="tw-flex tw-justify-between tw-w-full tw-mt-4 md:tw-mt-0 md:tw-w-1/2">
                    <div>
                        <span class="tw-block tw-text-sm tw-font-extrabold tw-uppercase">Countries</span>
                        <span class="tw-block tw-uppercase tw-text-center tw-text-5xl tw-font-bold">{{$package->no_of_countries}}</span>
                    </div>
                    <div>
                        <span class="tw-block tw-text-sm tw-font-extrabold tw-uppercase">Cities</span>
                        <span class="tw-block tw-uppercase tw-text-center tw-text-5xl tw-font-bold">{{$package->no_of_destinations}}</span>
                    </div>
                    <div>
                        <span class="tw-block tw-text-sm tw-font-extrabold tw-uppercase">Days</span>
                        <span class="tw-block tw-uppercase tw-text-center tw-text-5xl tw-font-bold">{{$package->no_of_days}}</span>
                    </div>
                    <div>
                        <span class="tw-block tw-text-sm tw-font-extrabold tw-uppercase">Starts From</span>
                        <span class="tw-block tw-text-5xl tw-uppercase tw-text-center tw-font-bold">
                            {!! $package->getCurrencyCode() !!}
                            {{floor($package->price_starts_from)}}
                        </span>
                    </div>
                    <div class="tw-hidden lg:tw-block">
                        <a class="tw-bg-primary-600 tw-block tw-outline-none focus:tw-outline-none tw-py-4 tw-px-8 tw-font-bold" href="#enquire-now-form">Enquire Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>