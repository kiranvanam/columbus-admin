<section class="tw-mt-16 scroll-margin-top-16" id="information">
    <div class="container tw-border-b">
        <div class="tw-mb-8"><h2 class="tw-text-8xl tw-font-thin">Tour Information</h2></div>
        @if($package->accommodations->count())
        <div class="tw-w-full tw-pb-8 tw-flex tw-flex-col md:tw-flex-row">
            <div class="tw-w-full md:tw-w-1/3">
                <h3 class="tw-text-2xl tw-font-black tw-mb-4">Accommodation</h3>
                <p class="tw-text-sm tw-text-gray-700">Enjoy a comfortable stay at the finest hotels at every destination handpicked by our travel experts. Under unavoidable circumstances hotels are subject to change. In such conditions a substitute hotel of similar category will be provided.</p>
            </div>
            <div class="tw-w-full md:tw-w-2/3 tw-p-4">
                <table class="tw-w-full tw-border tw-border-collapse">
                    <thead>
                        <tr>
                            <th class="tw-px-2 tw-py-2 tw-border">#</th>
                            <th class="tw-px-2 tw-py-2 tw-border">City - Country</th>
                            <th class="tw-px-2 tw-py-2 tw-border">Hotel</th>
                            <th class="tw-px-2 tw-py-2 tw-border"># Nights</th>
                        </tr>
                    </thead>
                    <tbody class="tw-text-sm">
                        @foreach($package->accommodations as $accomodation)
                        <tr class="@if($loop->iteration % 2) tw-bg-gray-200 @endif">
                            <td class="tw-text-center tw-px-2 tw-py-2 tw-border">{{$loop->iteration}}</td>
                            <td class="tw-text-center tw-px-2 tw-py-2 tw-border">{{$accomodation->city_name}} - {{$accomodation->country_name}}</td>
                            <td class="tw-text-center tw-px-2 tw-py-2 tw-border">{{$accomodation->hotel_name}}</td>
                            <td class="tw-text-center tw-px-2 tw-py-2 tw-border">{{$accomodation->no_of_nights}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endif
        <div class="tw-w-full">
            <div class="col-count-1 md:col-count-2 col-gap-12 tw-pb-8">
                @if(!empty($package->inclusions) || !empty($package->exclusions))
                <div class="avoid-break-inside">
                    @if(!empty($package->inclusions))
                    <div class="tw-w-full tw-pb-8">
                        <h3 class="tw-text-2xl tw-font-black tw-mb-4">Inclusions</h3>
                        <div class="tw-text-sm tw-leading-xloose information-section list-info">
                            {!! $package->inclusions !!}
                        </div>
                    </div>
                    @endif
                    @if(!empty($package->exclusions))
                    <div class="tw-w-full tw-pb-8 avoid-break-inside">
                        <h3 class="tw-text-2xl tw-font-black tw-mb-4">Exclusions</h3>
                        <div class="tw-text-sm tw-leading-xloose information-section list-info">
                            {!! $package->exclusions !!}
                        </div>
                    </div>
                    @endif
                </div>
                @endif
                @if(!empty($package->cancellation_policy))
                <div class="tw-w-full tw-pb-8 avoid-break-inside">
                    <h3 class="tw-text-2xl tw-font-black tw-mb-4">Cancellation Policy</h3>
                    <div class="tw-text-sm tw-leading-xloose information-section list-info">
                        {!! $package->cancellation_policy !!}
                    </div>
                </div>
                @endif
                @if(!empty($package->payment_policy))
                <div class="tw-w-full tw-pb-8 avoid-break-inside">
                    <h3 class="tw-text-2xl tw-font-black tw-mb-4">Payment Policy</h3>
                    <div class="tw-text-sm tw-leading-xloose information-section list-info">
                        {!! $package->payment_policy !!}
                    </div>
                </div>
                @endif
                @if(!empty($package->remarks))
                <div class="tw-w-full tw-pb-8 avoid-break-inside">
                    <h3 class="tw-text-2xl tw-font-black tw-mb-4">Remarks</h3>
                    <div class="tw-text-sm tw-leading-xloose information-section list-info">
                        {!! $package->remarks !!}
                    </div>
                </div>
                @endif
                @if(!empty($package->special_notes))
                <div class="tw-w-full tw-pb-8 avoid-break-inside">
                    <h3 class="tw-text-2xl tw-font-black tw-mb-4">Special Notes</h3>
                    <div class="tw-text-sm tw-leading-xloose information-section list-info">
                        {!! $package->special_notes !!}
                    </div>
                </div>
                @endif
            </div>
            @if(!empty($package->other_information))
            <div class="tw-w-full tw-pb-8 avoid-break-inside">
                <h3 class="tw-text-2xl tw-font-black tw-mb-4">Other Information</h3>
                <div class="tw-text-sm tw-leading-xloose information-section list-info">
                    {!! $package->other_information !!}
                </div>
            </div>
            @endif
        </div>
    </div>
</section>