@extends('website::layouts.app')

@section('content')

<!-- Main Section -->
<main class="tw-flex-grow">
    @component('website::components.packages.hero', ['package' => $package])
    @endcomponent
    <div class="tw-sticky tw-top-0 tw-bg-white tw-z-10">
        <div class="container tw-flex tw-justify-between tw-border-b">
            <ul class="tw-flex tw--mx-4" id="package-navigation-list">
                <li class="tw-px-4"><a href="#overview" id="nav-overview" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400 active">Overview</a></li>
                <li class="tw-px-4"><a href="#itinerary" id="nav-itinerary" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">Itinerary</a></li>
                <li class="tw-px-4"><a href="#information" id="nav-information" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">Information</a></li>
                @if($package->faqs->count())
                <li class="tw-px-4"><a href="#faq" id="nav-faq" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">FAQs</a></li>
                @endif
                <li class="tw-px-4"><a href="#dates-prices" id="nav-dates-prices" style="line-height: 42px; height:51px;" class="tw-inline-block tw-py-1 tw-border-b-4 tw-border-transparent hover:tw-border-gray-400">Dates & Price</a></li>
            </ul>
        </div>
    </div>
    <article>
        <section class="scroll-margin-top-16" id="overview">
            <div class="container tw-border-b tw-pb-16">
                <div class="tw-flex tw--mx-8 tw-pt-8">
                    <div class="tw-w-full md:tw-w-2/3 tw-px-8">
                        <!--
                        <div class="tw-flex tw-mb-4">
                            <div class="tw-mr-4">
                                <span>Tour Type </span>
                                <span class="tw-font-bold">{{$package->tour_type}}</span>
                            </div>
                            <div>
                                <span class="tw-text-lg">&bull;</span>
                            </div>
                            <div class="tw-ml-4">
                                <span>Tour Code </span>
                                <span class="tw-font-bold">{{$package->tour_code}}</span>
                            </div>
                        </div>
                        -->
                        <p class="tw-leading-loose tw-mb-8 tw-text-justify tw-font-weight">
                            {!! $package->overview !!}
                        </p>
                        <div class="tw-flex tw-flex-wrap">
                            <figure class="tw-w-full sm:tw-w-1/2 lg:tw-w-7/12 xl:tw-w-5/12">
                                <img class="tw-h-auto tw-w-full" src="{{$package->overview_image}}" alt="{{$package->title}}">
                            </figure>
                            <div class="tw-w-full sm:tw-w-1/2 lg:tw-w-5/12 xl:tw-w-7/12 tw-pl-8">
                                <h2 class="tw-mb-4">Tour Highlights</h2>
                                <div class="list-info tw-font-bold tw-text-sm">
                                    {!! $package->tour_highlights !!}
                                </div>
                            </div>
                        </div>
                        <div class="tw-mt-16">
                            <span class="tw-font-bold tw-block tw-mt-2">Notes</span>
                            <div class="list-info tw-text-sm">
                                {!! $package->important_notes !!}
                            </div>
                        </div>
                    </div>
                    <div class="tw-hidden md:tw-block md:tw-w-1/3 tw-px-8" id="enquire-now-form">
                        <div class="tw-text-sm">
                            <div class="tw-bg-primary-600 tw-px-4 tw-pb-4 tw-pt-8 tw-text-white tw-rounded-t-lg">
                                <h3 class="tw-text-lg tw-font-bold tw-mb-2">Enquire Now</h3>
                                <p class="tw-text-sm">Get in touch with our travel experts to help customize your trip as per your needs!</p>
                            </div>
                            <div class="tw-p-4 tw-border tw-rounded-b-lg">
                                <div>
                                    <label for="name" class="tw-block tw-font-semibold tw-mb-2">Full Name</label>
                                    <input class="tw-appearance-none focus:tw-outline-none focus:tw-border-primary-600 tw-border tw-rounded tw-w-full tw-py-2 tw-px-3" type="text" name="full_name" id="name">
                                </div>
                                <div class="tw-mt-2">
                                    <label for="email" class="tw-block tw-font-semibold tw-mb-2">Residence Country</label>
                                    <div class="tw-relative">
                                        <select name="country" id="country" class="tw-block tw-appearance-none tw-w-full tw-border tw-py-3 tw-px-4 tw-rounded focus:tw-outline-none">
                                            <option value="IN">India</option>
                                            <option value="US">United States of America</option>
                                            <option value="UK">United Kingdom</option>
                                        </select>
                                        <div class="tw-pointer-events-none tw-absolute tw-inset-y-0 tw-right-0 tw-flex tw-items-center tw-px-2">
                                            <svg class="icon icon-chevron-down"><use xlink:href="#icon-chevron-down"></use></svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="tw-mt-2">
                                    <label for="tel" class="tw-block tw-font-semibold tw-mb-2">Phone #</label>
                                    <input class="tw-appearance-none focus:tw-outline-none focus:tw-border-primary-600 tw-border tw-rounded tw-w-full tw-py-2 tw-px-3" type="tel" name="tel" id="tel">
                                </div>
                                <div class="tw-mt-2">
                                    <label for="email" class="tw-block tw-font-semibold tw-mb-2">Email</label>
                                    <input class="tw-appearance-none focus:tw-outline-none focus:tw-border-primary-600 tw-border tw-rounded tw-w-full tw-py-2 tw-px-3" type="email" name="email" id="email">
                                </div>
                                <div class="tw-mt-4">
                                    <button class="tw-appearance-none focus:tw-outline-none tw-bg-primary-600 tw-text-white tw-px-4 tw-py-2 tw-rounded" tyep="button">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="tw-mt-16 scroll-margin-top-16" id="itinerary">
            <div class="container tw-pb-8 tw-border-b">
                <div class="tw-flex tw-text-teal-600 tw-text-sm tw-font-bold">
                    <div class="tw-mr-4">
                        <svg class="icon icon-download"><use xlink:href="#icon-download"></use></svg>
                        <a href="" class="hover:tw-underline">Download</a>
                    </div>
                    <div class="tw-mr-4">
                        <svg class="icon icon-print"><use xlink:href="#icon-print"></use></svg>
                        <a href="" class="hover:tw-underline">Print</a>
                    </div>
                    <div>
                        <svg class="icon icon-mail"><use xlink:href="#icon-mail"></use></svg>
                        <a href="" class="hover:tw-underline">Email</a>
                    </div>
                </div>
                <div class="tw-mt-2"><h2 class="tw-text-8xl tw-font-thin">Your Itinerary</h2></div>
                <div class="tw-mt-8 tw-flex tw-flex-nowrap tw--mr-8">
                    <div class="tw-w-full md:tw-w-2/3 tw-pr-8">
                        @component('website::components.packages.itinerary', ['package' => $package])
                        @endcomponent
                    </div>
                    <aside class="tw-hidden md:tw-block md:tw-w-1/3 md:tw-pr-8">
                        @component('website::components.packages.side-content', ['package' => $package])
                        @endcomponent
                    </aside>
                </div>
            </div>
        </section>
        @component('website::components.packages.information', ['package' => $package])
        @endcomponent

        @if($package->faqs->count())
        @component('website::components.packages.faqs', ['package' => $package])
        @endcomponent
        @endif

        <section class="tw-mt-16 scroll-margin-top-16" id="dates-prices">
        </section>
    </article>
    <section class="tw-pb-12 tw-mt-16">
        <div class="container">
            <img class="tw-mx-auto tw-block" src="/images/heart.webp">
            <h3 class="tw-text-center">Let’s stitch travel dreams together!</h3>
        </div>
    </section>
    <div class="tw-fixed md:tw-hidden tw-w-full tw-left-0 tw-bottom-0 tw-bg-white">
        <button type="button" class="tw-bg-primary-600 tw-cursor-pointer tw-w-full tw-block tw-font-bold tw-text-center tw-uppercase tw-h-12 tw-border-t focus:tw-outline-none tw-text-white">Enquire Now</button>
    </div>
</main>
<!-- End Of Main Section -->

@endsection