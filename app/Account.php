<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'email', 'phone', 'address', 'city', 'region', 'country', 'postal_code', 'type', 'referred_by'];

    public function contacts() {
        return $this->hasMany(Contact::class);
    }

    public function scopeFilter($query, array $filters) {
        $query->when($filters['search'] ?? null, function($query, $search) {
            $query->where('name', 'like', '%'.$search.'%');
        })->when($filters['trashed'] ?? null, function($query, $trashed) {
            if ($trashed === 'with') {
                $query->withTrashed();
            } elseif ($trashed === 'only') {
                $query->onlyTrashed();
            }
        });
    }
}
