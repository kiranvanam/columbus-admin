<?php

namespace App;

use App\Traits\Packages\PackageDestinationsUpdateTrait;
use App\Traits\Packages\PackageItinerariesUpdateTrait;
use App\Traits\Packages\PackageAccommodationsUpdateTrait;
use App\Traits\Packages\PackageFiltersUpdateTrait;
use App\Traits\Packages\PackageFAQsUpdateTrait;
use App\Traits\Packages\PackageHelpersTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes, PackageDestinationsUpdateTrait, PackageItinerariesUpdateTrait,
        PackageAccommodationsUpdateTrait, PackageFiltersUpdateTrait, PackageFAQsUpdateTrait, 
        PackageHelpersTrait;

    protected $guarded = [];

    protected $casts = [
        'day_wise_destinations' => 'array'
    ];

    public function scopeFilter($query, array $filters) {
        $query->when($filters['search'] ?? null, function($query, $search) {
            $query->where('name', 'like', '%'.$search.'%');
        })->when($filters['trashed'] ?? null, function($query, $trashed) {
            if ($trashed === 'with') {
                $query->withTrashed();
            } elseif ($trashed === 'only') {
                $query->onlyTrashed();
            }
        });
    }

    public function destinations () {
        return $this->belongsToMany(Destination::class);
    }

    public function itineraries () {
        return $this->hasMany(PackageItinerary::class)->orderBy('day_number');
    }

    public function accommodations () {
        return $this->hasMany(PackageAccommodation::class)->orderBy('order');
    }

    public function filters () {
        return $this->morphToMany(Filter::class, 'filterable');
    }

    public function faqs() {
        return $this->morphMany(FAQ::class, 'faqable');
    }

    public function getMorphClass() {
        return 'packages';
    }
}
