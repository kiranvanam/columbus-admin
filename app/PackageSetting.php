<?php

namespace App;

class PackageSetting extends Model
{
    protected $guarded = [];

    public $timestamps = false;
}
