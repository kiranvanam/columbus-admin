<?php

namespace App\Traits\Packages;

use App\Filter;
use App\Currency;

trait PackageHelpersTrait {
    public function getPackageFilterByType($type) {
        $packageFilters = $this->filters;
        $packageFilterIDs = $packageFilters->pluck('id')->toArray();
        $filter = Filter::where('slug', $type)->first();
        $children = $filter->children;
        $childrenIDs = $children->pluck('id')->toArray();
        $packageFiltersByType = array_values(array_intersect($packageFilterIDs, $childrenIDs));
        $packageFiltersByTypeID = count($packageFiltersByType) ? $packageFiltersByType[0] : null;
        return $packageFiltersByTypeID ? $children[array_search($packageFiltersByTypeID, array_column($children->toArray(), 'id'))] : null;
    }

    public function getTourType() {
        return $this->getPackageFilterByType('tour-type');
    }

    public function getTourTypeName() {
        $packageTourType = $this->getTourType();
        return $packageTourType ? $packageTourType->name : null;
    }

    public function getTourServiceStandard() {
        return $this->getPackageFilterByType('service');
    }

    public function getTourServiceStandardName() {
        $packageServiceStandard = $this->getTourServiceStandard();
        return $packageTourType ? $packageServiceStandard->name : null;
    }

    public function isGold() {
        return $this->checkServiceStandard('premium');
    }

    public function isPlatinum() {
        return $this->checkServiceStandard('luxury');
    }

    public function checkServiceStandard($service) {
        $serviceFilter = $this->getTourServiceStandard();
        return ($serviceFilter && $serviceFilter->slug == strtolower($service)) ? true : false;
    }

    public function getCurrencyCode() {
        $currency = Currency::where('symbol', $this->price_starts_from_currency)->first();
        return empty(trim($currency->code)) ? '' : substr($currency->code, 0, strpos($currency->code, ";") + 1);
    }
}