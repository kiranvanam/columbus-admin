<?php

namespace App\Traits\Packages;

use Illuminate\Support\Arr;
use App\Destination;
use App\PackageAccommodation;

trait PackageAccommodationsUpdateTrait {
    public function updateAccommodationDetails($requestAccommodationDetails) {
        $historyAccommodationDetails = $this->accommodations->toArray();
        // Set the order of the accommodation details
        for($index=0; $index<count($requestAccommodationDetails); $index++) {
            $requestAccommodationDetails[$index]['order'] = $index + 1;
        }
        $changes = $this->splitAccommodationDetailsBasedOnTheOperationRequired($requestAccommodationDetails, $historyAccommodationDetails);
        $this->accommodations()->createMany($changes['create']);
        $this->updateMultipleAccommodationDetails($changes['update']);
        PackageAccommodation::destroy($changes['delete']);
    }

    protected function splitAccommodationDetailsBasedOnTheOperationRequired($requestAccommodationDetails, $historyAccommodationDetails) {
        $changes['create'] = Arr::where($requestAccommodationDetails, function ($accommodation) {
            return !array_key_exists('id', $accommodation);
        });
        $changes['update'] = Arr::where($requestAccommodationDetails, function ($accommodation) {
            return array_key_exists('id', $accommodation);
        });
        $historyAccommodationIDs = Arr::pluck($historyAccommodationDetails, 'id');
        $requestAccommodationIDs = Arr::pluck($changes['update'], 'id');
        $changes['delete'] = Arr::where($historyAccommodationIDs, function ($historyAccommodationID) use ($requestAccommodationIDs) {
            return !in_array($historyAccommodationID, $requestAccommodationIDs);
        });
        return $changes;
    }

    protected function updateMultipleAccommodationDetails($updateAccommodationDetails) {
        $accommodationIDs = Arr::pluck($updateAccommodationDetails, 'id');
        $packageAccommodations = PackageAccommodation::whereIn('id', $accommodationIDs)->get();
        foreach($packageAccommodations as $packageAccommodation) {
            $packageAccommodationDetails = Arr::first($updateAccommodationDetails, function ($accommodationDetail) use ($packageAccommodation) {
                return $accommodationDetail['id'] === $packageAccommodation->id;
            });
            $packageAccommodation->city_name = $packageAccommodationDetails['city_name'];
            $packageAccommodation->country_name = $packageAccommodationDetails['country_name'];
            $packageAccommodation->order = $packageAccommodationDetails['order'];
            $packageAccommodation->no_of_nights = $packageAccommodationDetails['no_of_nights'];
            $packageAccommodation->hotel_name = $packageAccommodationDetails['hotel_name'];
            $packageAccommodation->room_category = $packageAccommodationDetails['room_category'];
            $packageAccommodation->destination_id = $packageAccommodationDetails['destination_id'];
            $packageAccommodation->save();
        }
    }
}