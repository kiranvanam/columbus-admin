<?php

namespace App\Traits\Packages;

use Illuminate\Support\Arr;
use App\FAQ;
use Illuminate\Support\Facades\Auth;


trait PackageFAQsUpdateTrait {
    public function updateFAQDetails($requestFAQDetails) {
        $userID = Auth::id();
        if($userID) {
            $requestFAQDetails = array_map(function ($faq) use ($userID) {
                $faq['user_id'] = $userID;
                return $faq;
            }, $requestFAQDetails);
        }
        $historyFAQDetails = $this->faqs->toArray();
        $changes = $this->splitFAQDetailsBasedOnTheOperationRequired($requestFAQDetails, $historyFAQDetails);
        // Create new FAQs
        $this->faqs()->createMany($changes['create']);
        // Update existing FAQs
        $this->updateMultipleFAQDetails($changes['update']);
        // Delete removed FAQs
        FAQ::destroy($changes['delete']);
    }

    protected function splitFAQDetailsBasedOnTheOperationRequired($requestFAQDetails, $historyFAQDetails) {
        // Get newly added FAQs
        $changes['create'] = Arr::where($requestFAQDetails, function ($faq) {
            return !array_key_exists('id', $faq);
        });
        // Get existing FAQs
        $changes['update'] = Arr::where($requestFAQDetails, function ($faq) {
            return array_key_exists('id', $faq);
        });
        // Get FAQs that are no longer needed
        $historyFAQIDs = Arr::pluck($historyFAQDetails, 'id');
        $requestFAQIDs = Arr::pluck($changes['update'], 'id');
        $changes['delete'] = Arr::where($historyFAQIDs, function ($historyFAQID) use ($requestFAQIDs) {
            return !in_array($historyFAQID, $requestFAQIDs);
        });
        return $changes;
    }

    protected function updateMultipleFAQDetails($updateFAQDetails) {
        $faqIDs = Arr::pluck($updateFAQDetails, 'id');
        $packageFAQs = FAQ::whereIn('id', $faqIDs)->get();
        foreach($packageFAQs as $packageFAQ) {
            $packageFAQDetails = Arr::first($updateFAQDetails, function ($faqDetail) use ($packageFAQ) {
                return $faqDetail['id'] === $packageFAQ->id;
            });
            $packageFAQ->question = $packageFAQDetails['question'];
            $packageFAQ->answer = $packageFAQDetails['answer'];
            $packageFAQ->save();
        }
    }
}