<?php

namespace App\Traits\Packages;

trait PackageFiltersUpdateTrait {
    public function updateFilterDetails($filters) {
        $this->filters()->sync($filters);
    }
}