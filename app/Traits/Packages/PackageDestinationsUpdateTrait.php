<?php

namespace App\Traits\Packages;

use App\Destination;

trait PackageDestinationsUpdateTrait {
    public function updateDestinationDetails($destinationDetails) {
        $this->update($destinationDetails);
        $this->updateCountryAndDestinationCount();
    }

    protected function updateCountryAndDestinationCount() {
        $uniqueDestinationIDs = array_unique($this->getDestinationIDs());
        $this->destinations()->sync($uniqueDestinationIDs);
        // Update the number of countries and destinations visited.
        $this->update([
            'no_of_countries' => count(array_unique($this->getCountryIDs($uniqueDestinationIDs))),
            'no_of_destinations' => count($uniqueDestinationIDs)
        ]);
    }

    protected function getDestinationIDs() {
        $destinationIDs = [];
        $dayWiseDestinations = $this->day_wise_destinations;
        for($dayIndex = 0; $dayIndex < count($dayWiseDestinations); $dayIndex++) {
            $day = $dayWiseDestinations[$dayIndex];
            for($destinationIndex = 0; $destinationIndex < count($day['destinations']); $destinationIndex++) {
                $destination = $day['destinations'][$destinationIndex];
                array_push($destinationIDs, $destination['destination_id']);
            }
        }
        return $destinationIDs;
    }

    protected function getCountryIDs($uniqueDestinationIDs) {
        $countries = [];
        $destinations = Destination::with('country')->whereIn('id', $uniqueDestinationIDs)->get();
        for($index = 0; $index < count($destinations); $index++) {
            array_push($countries, $destinations[$index]['country']['id']);
        }
        return $countries;
    }
}