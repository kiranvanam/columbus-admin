<?php

namespace App\Traits\Packages;

use App\Destination;

trait PackageItinerariesUpdateTrait {
    public function updatePackageItineraries($itineraries) {
        $this->itineraries()->delete();
        $this->itineraries()->createMany($itineraries);
    }
}