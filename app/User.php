<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use SoftDeletes, Authenticatable, Authorizable;

    protected $fillable = [
            'title', 'first_name', 'last_name', 'phone', 'email', 'password',
            'user_type', 'city', 'region', 'country', 'address', 'postal_code',
            'remember_token'
        ];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = [
        //'owner' => 'boolean',
    ];

    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function scopeOrderByName($query)
    {
        $query->orderBy('last_name')->orderBy('first_name');
    }

    public function scopeWhereRole($query, $role)
    {
        switch ($role) {
            case 'user': return $query->where('owner', false);
            case 'owner': return $query->where('owner', true);
        }
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('first_name', 'like', '%'.$search.'%')
                    ->orWhere('last_name', 'like', '%'.$search.'%')
                    ->orWhere('email', 'like', '%'.$search.'%');
            });
        })->when($filters['role'] ?? null, function ($query, $role) {
            $query->whereRole($role);
        })->when($filters['trashed'] ?? null, function ($query, $trashed) {
            if ($trashed === 'with') {
                $query->withTrashed();
            } elseif ($trashed === 'only') {
                $query->onlyTrashed();
            }
        });
    }

    public static function addCustomer($userDetails)
    {
        $userDetails["user_type"] = "customer";

        // if no email given, email = phone
        if( empty($userDetails["email"]))
            $userDetails["email"] =  $userDetails["phone"];

        //default password - CV1@354
        if(empty($userDetails["password"]))
            $userDetails["password"] =  array_get($userDetails, "phone", "CV1@354");

        return self::create($userDetails);
    }

    public function isCustomer()
    {
        return $this->user_type == 'customer';
    }

    public function isEmployee()
    {
        return $this->user_type == 'employee';
    }
}
