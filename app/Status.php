<?php

namespace App;

class Status extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'type'];

    public static function getStatusesByType($type = 'generic') {
        $statuses = Status::where('type', $type)->get();
        return $statuses;
    }

    public static function inquiryStatuses()
    {
        return static::getStatusesByType('inquiry');
    }

    public function isPostPoned()
    {
        return $this->slug == "postponed";
    }

    public function isCancelled()
    {
        return $this->slug == "cancelled-program";
    }

    public function isBooked()
    {
        return $this->slug == 'booked';
    }


    public function isLeadLost()
    {
        return $this->slug == "lead-lost";
    }
}
