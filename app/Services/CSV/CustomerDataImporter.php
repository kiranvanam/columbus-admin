<?php

namespace App\Services\CSV;

use App\User;

class CustomerDataImporter
{
    function insertRecordsForFile($file)
    {
        $headers = ["title","first_name","last_name","phone","email","city","state","country","address","postal code"];
        $fileReader = new CSVFileReader($file, $headers);

        //return $fileReader->getItemsArray();
        $mobile = "111111111";

        $isFirstAvoid = true;

        foreach($fileReader->getItems() as $customer) {

            if(!$isFirstAvoid){

                if( empty($customer['title']))
                    $customer['title'] = "Mr";

                if( empty($customer['phone']) )
                    $customer['phone'] = $mobile++;

                if(empty($customer['email']))
                    $customer['email'] = $customer['phone'];

                $user = User::where('email', $customer['email'])
                            ->orwhere('phone', $customer['phone'])
                            ->first();

                if( $user) {
                    logger("Found User", [ 'phone'=> $user->phone, 'email' => $user->email ]);
                }else{
                    if( empty($customer['last_name']))
                        $customer['last_name'] = "";
                    $customer['country'] = "IN";
                    $customer['password'] = "";
                    $customer['user_type'] = "customer";
                    User::create($customer);
                }
            }else{
                $isFirstAvoid = false;
            }
        }
    }
}