<?php

namespace App\Services\SMS;

use GuzzleHttp\Client;

class SMSStriker
{
    protected $customer = "";
    protected $employee = "";
    protected $inquiry = "";

    public function sendSMSForInquiry($inquiry)
    {
        $this->customer = $inquiry->customer;
        $this->employee = $inquiry->assignedto;
        $this->inquiry = $inquiry;

        $client = new Client();
        $url = "https://www.smsstriker.com/API/multi_messages.php?".
                "username=columbusvacations&password=Cv@12345".
                "&from=CVSINQ". // CVLPAY, CVBCNF
                "&mno_msg=". $this->messageForCustomer() . "~". $this->messageForEmployee();
        //"https://www.smsstriker.com/API/sms.php?username=columbusvacations&password=Cv@12345&from=&to=[xxxxxxxxxx]&msg=[xxxx]&type=1";
        $result = $client->get($url);

        logger("*********** SMS Inquiry - ". $inquiry->code ." *************");
        logger("SMS Request url: ". $url);
        logger("SMS response: " .(string)$result->getBody());
        logger("*********** SMS Inquiry - ". $inquiry->code ." *************");

    }

    function messageForCustomer()
    {
        return $this->customerNumber()."^".$this->customerMessage();;
    }

    private function customerMessage()
    {
        $title = strtolower($this->customer->title) == 'mr' ? 'Sir' : 'Madam';
        $employee = $this->employeeNameAndPhone();

        return "Dear $title,%0A"
               . "Thank you for inquiring with us. $employee , will get back to you soon.%0A"
               . "Thank you,%0A"
               . "Columbus Vacations Team.";
    }

    private function customerNumber()
    {
        return $this->customer->phone;
    }

    private function messageForEmployee()
    {
        return $this->employeePhone()."^".$this->employeeMessage();
    }

    private function employeeMessage()
    {
        return "Dear " . $this->employeeName() . "%0A" .
                "You have been an assigned with an inquriy (". $this->inquiry->code . ")";
    }

    private function employeeNameAndPhone()
    {
        return $this->employeeName() . "- " . $this->employeePhone();
    }

    private function employeePhone()
    {
        return $this->employee->phone;
    }

    private function employeeName()
    {
        return $this->employee->title . " " . $this->employee->first_name;
    }
}