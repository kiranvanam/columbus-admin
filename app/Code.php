<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Code extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['module_name', 'code_prefix', 'start_code', 'last_used_code'];

    public static function getNextCodeForModule($module_name)
    {
        $code = self::nextCodeForModule($module_name);
        return $code->code_prefix . "-" . $code->last_used_code;
    }

    public static function nextCodeForModule($module_name) 
    {
        return self::updateLastUsedCode(self::getCodeRecordForModule( ['module_name' => $module_name]));
    }

    protected static function getCodeRecordForModule($data=[])
    {
        return static::where('module_name', $data['module_name'])->first();
    }

    public static function createCodeRecordWithData($data)
    {
        $code = self::getCodeRecordForModule($data);

        //initially start code & last used code are both same
        if(!$code) {
            $data['last_used_code'] = $data['start_code'];
            $code = self::create($data);
        }

        return $code;
    }


    public static function updateLastUsedCode($code)
    {
        if($code) {
            $last_used_code = $code->last_used_code;
            if( empty( $last_used_code) ) {
                $last_used_code = $code->start_code;
            }else {
                $last_used_code = ++$code->last_used_code;
            }
            $code->last_used_code = str_pad($last_used_code, strlen($code->start_code), 0, STR_PAD_LEFT);
            $code->save();
        }

        return $code;
    }
    
    public static function nextCodeForInquiry()
    {
        $module_name = "inquiries";
        $code = self::nextCodeForModule($module_name);
        return $code->code_prefix . "-" . $code->last_used_code;
    }

}
