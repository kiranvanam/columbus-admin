<?php

namespace App\Channels;

use App\Services\SMS\SMSStriker;
use App\User;
use Illuminate\Notifications\Notification;

class SMSChannel
{
    public function send($inquiry, Notification $inquiryCreated)
    {
        //var_dump($notifiable->name, $notification->inquiry->name);
        //$message = $notification->toSMS($notifiable);
        $smsStriker = new SMSStriker();
        $smsStriker->sendSMSForInquiry($inquiry);
    }
}
