<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = ['account_id', 'title', 'fname', 'lname', 'dob', 'email', 'alt_email', 'phone', 'alt_phone', 'address', 'city', 'region', 'country', 'postal_code', 'role'];

    public function account() {
        return $this->belongsTo(Account::class);
    }

    public  static function addNewContact($contactDetails) {
        // create account with surname + first_name
        $account_name = array_get($contactDetails, 'lname', ' ') . " " . array_get($contactDetails, 'fname', ' ');
        $account = Account::create( ['name' => $account_name] );
        $contactDetails['account_id'] = $account->id;
        return self::create($contactDetails);
    }

    public function scopeOrderByName($query) {
        return $query->orderBy('lname')->orderBy('fname');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where(function ($query) use ($search) {
                $query->where('fname', 'like', '%'.$search.'%')
                    ->orWhere('lname', 'like', '%'.$search.'%')
                    ->orWhere('email', 'like', '%'.$search.'%')
                    ->orWhereHas('account', function ($query) use ($search) {
                        $query->where('name', 'like', '%'.$search.'%');
                    });
            });
        })->when($filters['trashed'] ?? null, function ($query, $trashed) {
            if ($trashed === 'with') {
                $query->withTrashed();
            } elseif ($trashed === 'only') {
                $query->onlyTrashed();
            }
        });
    }
}