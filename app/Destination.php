<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;

class Destination extends Model
{
    use SoftDeletes;

    protected $fillable = ['country_code', 'name', 'slug', 'code', 'type', 'searchable_terms', 'short_description', 'long_description', 'lat', 'lng'];

    public function scopeFilter($query, $searchQuery) {
        $query->where('name', 'like', '%'.$searchQuery.'%')
            ->orWhere('searchable_terms', 'like', '%'.$searchQuery.'%');
    }

    public function country () {
        return $this->belongsTo(Country::class, 'country_code', 'cca2');
    }

    public function collections() {
        return $this->morphToMany(Collection::class, 'collectionable');
    }

    public function packages () {
        return $this->belongsToMany(Package::class);
    }
}