<?php

namespace App\Observers;

use App\Destination;
use App\Collection;

class DestinationObserver
{
    /**
     * Handle the destination "created" event.
     *
     * @param  \App\Destination  $destination
     * @return void
     */
    public function created(Destination $destination)
    {
        $collection = new Collection([
            'name' => $destination->name,
            'slug' => str_slug($destination->name),
            'short_description' => $destination->short_description,
            'long_description' => $destination->long_description,
            'has_countries' => false,
            'has_destinations' => true,
            'has_packages' => false,
        ]);
        $destination->collections()->save($collection);
        return;
    }

    /**
     * Handle the destination "updated" event.
     *
     * @param  \App\Destination  $destination
     * @return void
     */
    public function updated(Destination $destination)
    {
    }

    /**
     * Handle the destination "deleted" event.
     *
     * @param  \App\Destination  $destination
     * @return void
     */
    public function deleted(Destination $destination)
    {

    }

    /**
     * Handle the destination "restored" event.
     *
     * @param  \App\Destination  $destination
     * @return void
     */
    public function restored(Destination $destination)
    {
    }

    /**
     * Handle the destination "force deleted" event.
     *
     * @param  \App\Destination  $destination
     * @return void
     */
    public function forceDeleted(Destination $destination)
    {
        //
    }
}
