<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;

class UserContact implements FromArray
{
    public  $userContacts = [];

    public function __construct()
    {
        $this->userContacts = \App\User::where('phone', 'like', '9%')
                                        ->orWhere('phone', 'like', '8%')
                                        ->orWhere('phone', 'like', '7%')
                                        ->orWhere('phone', 'like', '6%')
                                        ->orWhere('phone', 'like', '+91%')
                                        ->select('phone')
                                        ->get()
                                        ->filter(function($user) {
                                            $searchFor = [' ', '+91-', '+91'];
                                            $user->phone = trim(str_replace($searchFor , '', $user->phone));
                                            return  strlen($user->phone) == 10;
                                        });
                                        
    }

    public function array():array
    {
        return  $this->userContacts->toArray();
    }
}
