<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Inquiry extends Model
{
    use SoftDeletes;
    use Notifiable;

    protected $fillable = ['name', 'inquiry_type', 'code', 'date_of_travel', 'package',
                    'status_id', 'notes', 'created_by_customer',
                    'customer_id', 'assigned_to', 'created_by', 'closed_notes',
                    'lead_source_type', 'lead_source_name', 'lead_source_user_id',
                    'priority', 'closed_on'
                ];

    protected $casts = [
        'created_by_customer' => 'boolean',
        'notes' => 'array'
    ];

    public function customer() {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function assignedTo() {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function scopeFilter($query, array $filters) {
        $query->when($filters['search'] ?? null, function($query, $search) {
            $query->where('name', 'like', '%'.$search.'%');
        })
        ->when($filters['trashed'] ?? null, function($query, $trashed) {
            if ($trashed === 'with') {
                $query->withTrashed();
            } elseif ($trashed === 'only') {
                $query->onlyTrashed();
            }
        })
        ->when( $filters['status_ids'] ?? null, function($query, $status_ids){
            if( is_string($status_ids))
                $status_ids = explode(",", $status_ids);
            $query->whereIn('status_id', $status_ids);
        })
        ->when($filters['assigned_to'] ?? null, function($query, $assignedTo) {
            $query->where('assigned_to', $assignedTo);
        });
    }

    public function isPostPoned()
    {
        return optional(Status::find($this->status_id))->isPosePoned();
    }
}
