<?php

namespace App\Http\Controllers;

use App\Account;
use App\Country;
use App\Contact;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class AccountsController extends Controller
{
    public function index() {
        $accounts = Account::orderBy('name')->filter(Request::only('search', 'trashed'))->paginate();

        return Inertia::render('Accounts/Index', [
            'filters' => Request::all('search', 'trashed'),
            'accounts' => $accounts,
        ]);
    }

    public function create() {
        $countries = Country::all();
        return Inertia::render('Accounts/Create', [
            'countries' => $countries
        ]);
    }

    public function store() {
        $account = Account::create(
            Request::validate([
                'name' => ['required', 'max:100'],
                'email' => ['nullable', 'email', 'required_without:phone'],
                'phone' => ['nullable', 'required_without:email'],
                'address' => ['nullable', 'max:150'],
                'city' => ['nullable', 'max:50'],
                'region' => ['nullable', 'max:50'],
                'country' => ['nullable', 'max:2'],
                'postal_code' => ['nullable', 'max:25'],
                'type' => ['nullable', 'max:50'],
                'referred_by' => ['nullable']
            ])
        );
        return Redirect::route('accounts')->with('success', 'Account created.');
    }

    public function edit(Account $account)
    {
        $countries = Country::all();
        $referredBy = $this->getReferral($account);
        return Inertia::render('Accounts/Edit', [
            'account' => [
                'id' => $account->id,
                'name' => $account->name,
                'email' => $account->email,
                'phone' => $account->phone,
                'address' => $account->address,
                'city' => $account->city,
                'region' => $account->region,
                'country' => $account->country,
                'postal_code' => $account->postal_code,
                'type' => $account->type,
                'deleted_at' => $account->deleted_at,
                'contacts' => $account->contacts()->orderByName()->get(),
                'referred_by' => $account->referred_by,
                'referredBy' => $referredBy
            ],
            'countries' => $countries,
        ]);
    }

    protected function getReferral($account) {
        return $account->referred_by ? Contact::find($account->referred_by) : null;
    }

    public function update(Account $account)
    {
        $account->update(
            Request::validate([
                'name' => ['required', 'max:100'],
                'email' => ['nullable', 'email', 'required_without:phone'],
                'phone' => ['nullable', 'required_without:email'],
                'address' => ['nullable', 'max:150'],
                'city' => ['nullable', 'max:50'],
                'region' => ['nullable', 'max:50'],
                'country' => ['nullable', 'max:2'],
                'postal_code' => ['nullable', 'max:25'],
                'type' => ['nullable', 'max:50'],
                'referred_by' => ['nullable']
            ])
        );
        return Redirect::route('accounts.edit', $account)->with('success', 'Account updated.');
    }

    public function destroy(Account $account)
    {
        $account->delete();

        return Redirect::route('accounts.edit', $account)->with('success', 'Account deleted.');
    }

    public function restore(Account $account)
    {
        $account->restore();

        return Redirect::route('accounts.edit', $account)->with('success', 'Account restored.');
    }

    public function getAccounts()
    {
        $searchQuery = request('query', '');
        if(empty($searchQuery)) {
            return [];
        }
        $accounts = Account::where('name', 'like', '%'.$searchQuery.'%')->get();
        return $accounts;
    }
}
