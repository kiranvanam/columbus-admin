<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class FileUploadController extends Controller
{
    public function upload(Request $request) {
        if ($request->hasFile('file')) {
            $fileName = empty($request->fileName) ? $request->file->getClientOriginalName() : $request->fileName.'.'.$request->file->extension();
            $uploadPath = $request->uploadPath;
            $path = $request->file->storeAs($uploadPath, $fileName, 'public');
            return response([
                'path' => $path,
                'message' => 'File Uploaded Successfully'
            ], 200);
        }
        return response([
            'message' => 'No File To Upload'
        ], 400);
    }

    public function delete(Request $request) {
        $message = 'File Not Found';
        $code = 404;
        $pathToBeDeleted = $request['path'];
        if(Storage::disk('public')->exists($pathToBeDeleted)) {
            if(Storage::disk('public')->delete($pathToBeDeleted)) {
                $message = 'File Deleted Successfully';
                $code = 200;
                
            } else {
                $message = 'File Deletion Failed';
                $code = 500;
            }
        }
        return response([
            'message' => $message
        ], $code);
    }
}
