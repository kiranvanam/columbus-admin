<?php

namespace App\Http\Controllers;

use App\Code;
use Inertia\Inertia;

class CodeGenerationController
{

    public function index()
    {
        return Inertia::render('Codes/Index', [
                                'codes' => Code::all()
                            ]);
    }

    public function store()
    {
         $code = Code::createCodeRecordWithData( request()->all());

         return ['code' => $code];
             //$this->ok($module, "Code created for module - ". $module['module_name']);
    }

    public function nextCodeForModule($module_name)
    {
        return  [
            "code" => Code::nextCodeForModule($module_name),
            "message" => "Next Code generated"
        ];
    }
}