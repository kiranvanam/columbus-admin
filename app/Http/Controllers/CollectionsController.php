<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Country;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class CollectionsController extends Controller
{
    public function index() {
        $collections = Collection::filter(Request::only('search', 'trashed'))->paginate();

        return Inertia::render('Collections/Index', [
            'filters' => Request::all('search', 'trashed'),
            'collections' => $collections,
        ]);
    }

    public function create() {
        $servingCountries = Country::where('serving', true)->get();
        return Inertia::render('Collections/Create', [
            'servingCountries' => $servingCountries
        ]);
    }

    public function store() {
        $collection = Collection::create($this->getCollectionRequestData());
        $this->syncCountriesAndDestinations($collection);
        return Redirect::route('collections')->with('success', 'Collection created.');
    }

    public function edit(Collection $collection)
    {
        $countries = $collection->countries()->get();
        $destinations = $collection->destinations()->get();

        return Inertia::render('Collections/Edit', [
            'collection' => [
                'id' => $collection->id,
                'name' => $collection->name,
                'searchable_terms' => $collection->searchable_terms,
                'short_description' => $collection->short_description,
                'long_description' =>$collection->long_description
            ],
            'countries' => $countries,
            'destinations' => $destinations
        ]);
    }

    public function update(Collection $collection)
    {
        $collection->update($this->getCollectionRequestData());
        $this->syncCountriesAndDestinations($collection);
        return Redirect::route('collections.edit', $collection)->with('success', 'Collection updated.');
    }

    public function destroy(Collection $collection)
    {
        $collection->delete();
        return Redirect::route('collections.edit', $collection)->with('success', 'Collection deleted.');
    }

    public function restore(Collection $collection)
    {
        $collection->restore();
        return Redirect::route('collections.edit', $collection)->with('success', 'Collection restored.');
    }

    protected function syncCountriesAndDestinations($collection) {
        $syncDetails = Request::only('countries', 'destinations');
        $collection->countries()->sync($syncDetails['countries']);
        $collection->destinations()->sync($syncDetails['destinations']);
    }

    protected function getCollectionRequestData() {
        $collectionDetails = Request::validate([
            'name' => ['required'],
            'searchable_terms' => ['nullable'],
            'short_description' => ['nullable'],
            'long_description' => ['nullable'],
        ]);
        $collectionDetails['slug'] = str_slug($collectionDetails['name']);
        return $collectionDetails;
    }
}
