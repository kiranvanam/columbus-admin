<?php

namespace App\Http\Controllers;

use App\User;
use App\Country;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

class UsersController extends Controller
{

    public function index() {
        $users = User::orderByName()
                    ->filter(Request::only('search', 'role', 'trashed'))
                    ->where('user_type', 'employee')
                    ->paginate();

        return Inertia::render('Users/Index', [
            'filters' => Request::all('search', 'trashed'),
            'users' => $users,
        ]);
    }

    public function create() {
        $countries = Country::all();
        $users = User::all()->map->only('id', 'first_name', 'last_name', 'title');
        return Inertia::render('Users/Create', [
            'countries' => $countries,
            'users' => $users
        ]);
    }

    public function store() {
        $userDetails = Request::validate([
            'title' => ['required'],
            'reports_to' => ['nullable'],
            'first_name' => ['required', 'max:100'],
            'last_name' => ['nullable', 'max:100'],
            'email' => ['required', 'email'],
            'password' => ['required'],
            'phone' => ['required'],
            'personal_email' => ['required', 'email'],
            'personal_phone' => ['required'],
            'permanent_address' => ['required', 'max:150'],
            'current_address' => ['required', 'max:150'],
            'city' => ['required', 'max:50'],
            'region' => ['required', 'max:50'],
            'country' => ['required', 'max:2'],
            'postal_code' => ['nullable', 'max:25'],
            'owner' => ['required', 'boolean'],
        ]);

        $user = User::create($userDetails);
        return Redirect::route('users')->with('success', 'User created.');
    }

    public function edit(User $user) {
        $countries = Country::all();
        return Inertia::render('Users/Edit', [
            'countries' => $countries,
            'user' => $user
        ]);
    }

    public function update(User $user)
    {
        $user->update(
            Request::validate([
                'title' => ['required'],
                'first_name' => ['required', 'max:100'],
                'last_name' => ['nullable', 'max:100'],
                'email' => ['required', 'email'],
                'phone' => ['required'],
                'personal_email' => ['required', 'email'],
                'personal_phone' => ['required'],
                'permanent_address' => ['required', 'max:150'],
                'current_address' => ['required', 'max:150'],
                'city' => ['required', 'max:50'],
                'region' => ['required', 'max:50'],
                'country' => ['required', 'max:2'],
                'postal_code' => ['nullable', 'max:25'],
            ])
        );
        return Redirect::route('users.edit', $user)->with('success', 'User updated.');
    }

    public function destroy(User $user)
    {
        $user->delete();

        return Redirect::route('users.edit', $user)->with('success', 'User deleted.');
    }

    public function restore(User $user)
    {
        $user->restore();

        return Redirect::route('users.edit', $user)->with('success', 'User restored.');
    }

    public function getUsers()
    {
        $searchQuery = request('query', '');
        if(empty($searchQuery)) {
            return [];
        }
        \DB::connection()->enableQueryLog();

        $user_type = request('type');
        //correct-way where (`first_name` like ? or `last_name` like ?) and `user_type` = ? and `users`.`deleted_at` is null',
        //wrong-way  where (`first_name` like ? or `last_name` like ? and `user_type` = ?) and `users`.`deleted_at` is null',
        $users = User::where(function($query) use($searchQuery) {
                                $query->where('first_name', 'like', '%'.$searchQuery.'%')
                                    ->orWhere('last_name', 'like', '%'.$searchQuery.'%')
                                    ->orWhere('phone', 'like', '%'.$searchQuery.'%')
                                    ->orWhere('email', 'like', '%'.$searchQuery.'%');
                            });

        if( !empty($user_type) && $user_type != 'all') {
            $users->where('user_type', $user_type);
        }

        $users = $users->get(['id', 'first_name', 'last_name', 'email', 'phone', 'user_type']);
        logger(\DB::getQueryLog());
        return $users;
    }
}
