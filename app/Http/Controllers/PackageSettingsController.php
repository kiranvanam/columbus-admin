<?php

namespace App\Http\Controllers;

use App\Country;
use App\Destination;
use App\PackageSetting;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class PackageSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packageSettings = PackageSetting::all();
        return Inertia::render('Packages/Settings/Index', [
            'packageSettings' => $packageSettings,
        ]);
    }

    public function create() {
        return Inertia::render('Packages/Settings/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $settings = Request::validate([
            '*.title' => 'required',
            '*.description' => 'required',
        ]);
        $settings = array_map(function ($item) {
            $item['slug'] = str_slug($item['title']);
            return $item;
        }, $settings);
        PackageSetting::insert($settings);
        return Redirect::route('settings.packages')->with('success', 'Package settings created.');
    }

    public function edit(PackageSetting $packageSetting)
    {
        return Inertia::render('Packages/Settings/Create', [
            'packageSetting' => $packageSetting
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\PackageSetting  $packageSetting
     * @return \Illuminate\Http\Response
     */
    public function update(PackageSetting $packageSetting)
    {
        $packageSettingDetails = Request::validate([
            'title' => 'required',
            'description' => 'required'
        ]);
        $packageSettingDetails['slug'] = str_slug($packageSettingDetails['title']);
        $packageSetting->update($packageSettingDetails);
        return Redirect::route('settings.packages.edit', $packageSetting)->with('success', 'package settings updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageSetting  $packageSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackageSetting $packageSettings)
    {
        //
    }
}
