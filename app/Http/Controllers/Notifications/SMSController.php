<?php

namespace App\Http\Controllers\Notifications;

use Inertia\Inertia;
use Illuminate\Http\Request;

class SMSController extends Controller
{
    function index()
    {
        $smses = SMS::all();
        return Inertia::render('Notifications/SMS/Index', [
            'smses' => $smses
        ]);
    }
}
