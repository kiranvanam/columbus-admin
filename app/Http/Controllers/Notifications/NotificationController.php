<?php

namespace App\Http\Controllers\Notifications;

use Inertia\Inertia;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    function index()
    {
        return Inertia::render('Notifications/Index', []);
    }
}