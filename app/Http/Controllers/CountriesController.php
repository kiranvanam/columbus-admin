<?php

namespace App\Http\Controllers;

use App\Country;
use App\Observers\CountryObserver;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class CountriesController extends Controller
{
    public function index() {
        $filters = Request::only('search', 'serving');
        $countries = [];
        $servingCountries = Country::whereServing('serving')->select('name', 'id', 'cca2', 'serving')->get();
        if(array_key_exists('search', $filters) && (strlen($filters['search']) > 1)) {
            $countries = Country::filter($filters)->select('name', 'id', 'cca2', 'serving')->get();
        }

        return Inertia::render('Countries/Index', [
            'filters' => Request::all('search', 'serving'),
            'countries' => $countries,
            'servingCountries' => $servingCountries
        ]);
    }

    public function edit(Country $country) {
        return Inertia::render('Countries/Edit', [
            'country' => [
                'id' => $country->id,
                'name' => $country->name,
                'serving' => $country->serving,
                'searchable_terms' => $country->searchable_terms,
                'short_description' => $country->short_description,
                'long_description' => $country->long_description,
            ],
        ]);
    }

    public function update(Country $country) {
        $updateCountryDetails = Request::validate([
            'serving' => ['required'],
            'searchable_terms' => ['nullable'],
            'short_description' => ['nullable'],
            'long_description' => ['nullable'],
        ]);

        $country->update($updateCountryDetails);

        return Redirect::route('countries.edit', $country)->with('success', "Country updated");
    }

    public function getCountries() {
        $searchQuery = request('query', '');
        if(empty($searchQuery)) {
            return [];
        }
        $countries = Country::filter([
            'serving' => 'serving',
            'search' => $searchQuery
        ])->get();
        return $countries;
    }
}