<?php

namespace App\Http\Controllers;

use App\Country;
use App\Destination;
use App\Package;
use App\PackageSetting;
use App\Filter;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::filter(Request::only('search', 'trashed'))->paginate();

        return Inertia::render('Packages/Index', [
            'filters' => Request::all('search', 'trashed'),
            'packages' => $packages,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::where('serving', true)->get();
        return Inertia::render('Packages/Basic', [
            'countries' => $countries
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $packageBasicDetails = $this->getPackageBasicDetailsFromRequest();
        $package = Package::create($packageBasicDetails);
        return Redirect::route('packages')->with('success', 'Package created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    protected function getPackagesImagePath($images) {
        $imagePath = null;
        foreach($images as $image) {
            $imagePath = $image;
        }
        if($imagePath) {
            $imagePath = "/storage" . strstr($imagePath, '/packages');
        }
        return $imagePath;
    }

    protected function preparePackageDetailsToRender($package) {
        $countries = Country::where('serving', true)->get();
        $package['tour_starts_from'] = Destination::where('code', $package['tour_starts_from'])->first();
        $package['tour_ends_at'] = Destination::where('code', $package['tour_ends_at'])->first();
        $itinerary = $package->itineraries;
        $packageDestinations = $package->destinations;
        $packageAccommodations = $package->accommodations;
        $packageFilters = $package->filters;
        $packageSettings = PackageSetting::all();
        $filters = Filter::with('children')->whereNull('parent_id')->where('filter_type', 'package')->get();
        $faqs = $package->faqs;
        $image_exists = [
            'thumbnail' => $this->getPackagesImagePath(glob(storage_path("app/public/packages/{$package->id}/images/thumbnail.*"))),
            'banner' => $this->getPackagesImagePath(glob(storage_path("app/public/packages/{$package->id}/images/banner.*"))),
            'overview' => $this->getPackagesImagePath(glob(storage_path("app/public/packages/{$package->id}/images/overview.*")))
        ];
        return [
            'countries' => $countries,
            'tourPackage' => $package,
            'packageSettings' => $packageSettings,
            'filters' => $filters,
            'faqs' => $faqs,
            'imageExists' => $image_exists
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package, $componentName="Basic")
    {
        $packageDetails = $this->preparePackageDetailsToRender($package);
        return Inertia::render('Packages/'.$componentName, $packageDetails);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function editGallery(Package $package)
    {
        return $this->edit($package, 'Gallery');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function editDestinations(Package $package)
    {
        return $this->edit($package, 'Destinations');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function editItinerary(Package $package)
    {
        return $this->edit($package, 'Itinerary');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function editInformation(Package $package)
    {
        return $this->edit($package, 'Information');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function editAccommodations(Package $package)
    {
        return $this->edit($package, 'Accommodation');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function editFilters(Package $package)
    {
        return $this->edit($package, 'Filters');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function editFAQs(Package $package)
    {
        return $this->edit($package, 'FAQ');
    }

    public function updateDestinations(Package $package)
    {
        $destinationDetails = Request::validate([
            'day_wise_destinations' => 'required'
        ]);

        $package->updateDestinationDetails($destinationDetails);

        return Redirect::route('packages.destinations', [
            'tourPackage' => $package
        ]);
    }

    public function updateItinerary(Package $package)
    {
        $itineraries = Request::validate([
            'itineraries' => 'present|array',
            'itineraries.*.title' => 'required',
            'itineraries.*.description' => 'required',
            'itineraries.*.day_number' => 'required',
            'itineraries.*.day_highlights_tags' => 'nullable',
            'itineraries.*.additional_details' => 'nullable',
            'itineraries.*.meals' => 'nullable',
            'itineraries.*.overnight_stay' => 'nullable',
        ]);
        $package->updatePackageItineraries($itineraries['itineraries']);
        return Redirect::route('packages.itinerary', [
            'tourPackage' => $package
        ]);
    }

    public function updateAccommodations(Package $package)
    {
        $accommodationDetails = Request::validate([
            'accommodations' => 'required'
        ]);

        $package->updateAccommodationDetails($accommodationDetails['accommodations']);

        return Redirect::route('packages.accommodations', [
            'tourPackage' => $package
        ]);
    }

    public function updateInformation(Package $package)
    {
        $information = Request::validate([
            'payment_policy' => 'required',
            'cancellation_policy' => 'required',
            'remarks' => 'required',
            'special_notes' => 'nullable',
            'other_information' => 'nullable'
        ]);
        $package->update($information);
        return Redirect::route('packages.information', [
            'tourPackage' => $package
        ]);
    }

    public function updateFilters(Package $package)
    {
        $filterDetails = Request::validate([
            'filters' => 'present|array'
        ]);
        $package->updateFilterDetails($filterDetails['filters']);

        return Redirect::route('packages.filters', [
            'tourPackage' => $package
        ]);
    }

    public function updateFAQs(Package $package)
    {
        $faqDetails = Request::validate([
            'faqs' => 'present|array',
            'faqs.*.id' => 'nullable',
            'faqs.*.faqable_id' => 'nullable',
            'faqs.*.faqable_type' => 'nullable',
            'faqs.*.user_id' => 'nullable',
            'faqs.*.question' => 'required',
            'faqs.*.answer' => 'required',
            'faqs.*.approved' => 'required',
        ]);
        $package->updateFAQDetails($faqDetails['faqs']);

        return Redirect::route('packages.faqs', [
            'tourPackage' => $package
        ]);
    }

    public function updateGallery(Package $package)
    {
        $imageDetails = Request::validate([
            'path' => 'required',
            'image_name' => 'required'
        ]);

        $package->update([
            $imageDetails['image_name'] => $imageDetails['path']
        ]);

        return Redirect::route('packages.gallery', $package)->with('success', 'package updated.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        $packageBasicDetails = $this->getPackageBasicDetailsFromRequest($package['id']);
        $package->update($packageBasicDetails);

        return Redirect::route('packages.edit', $package)->with('success', 'package updated.');
    }

    protected function getPackageBasicDetailsFromRequest($packageID=0) {
        $requestDetails = Request::validate([
            'title' => [
                'required',
                'unique:packages,title,'.$packageID
            ],
            'tour_code' => [
                'required',
                'max:10',
                'unique:packages,tour_code,'.$packageID
            ],
            'banner_image' => ['required'],
            'overview_image' => ['required'],
            'price_starts_from_label' => ['nullable'],
            'price_starts_from_currency' => ['nullable'],
            'price_starts_from' => ['nullable'],
            'overview' => ['nullable'],
            'inclusions' => ['nullable'],
            'exclusions' => ['nullable'],
            'tour_highlights' => ['nullable'],
            'payment_policy' => ['nullable'],
            'special_notes' => ['nullable'],
            'remarks' => ['nullable'],
            'other_information' => ['nullable'],
            'operator' => ['nullable'],
            'tour_duration_format' => ['required'],
            'no_of_nights' => ['required'],
            'no_of_days' => ['required'],
            'no_of_hours' => ['required'],
            'no_of_minutes' => ['required'],
            'custom_duration' => ['nullable'],
            'min_age' => ['required'],
            'max_age' => ['required'],
            'min_pax' => ['required'],
            'max_pax' => ['required'],
            'tour_starts_from' => ['required'],
            'tour_ends_at' => ['required'],
            'day_wise_destinations' => ['nullable'],
            'important_notes' => ['nullable']
        ]);
        $requestDetails['slug'] = str_slug($requestDetails['title']);
        $requestDetails['tour_starts_from'] = $requestDetails['tour_starts_from']['code'];
        $requestDetails['tour_ends_at'] = $requestDetails['tour_ends_at']['code'];

        return $requestDetails;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        //
    }

    /**
     * Clone the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function clone(Package $package)
    {   
        $requestDetails = Request::validate([
            'title' => [
                'required',
                'unique:packages,title,0'
            ],
            'tour_code' => [
                'required',
                'max:10',
                'unique:packages,tour_code,0'
            ]
        ]);
        $newPackage = $package->replicate();
        $newPackage['title'] = $requestDetails['title'];
        $newPackage['slug'] = str_slug($requestDetails['title']);
        $newPackage['tour_code'] = $requestDetails['tour_code'];
        $newPackage->push();
        $package->load('destinations', 'itineraries', 'accommodations', 'filters', 'faqs');

        foreach ($package->relations as $relationName => $values){
            $newPackage->{$relationName}()->sync($values);
        }
        
        return Redirect::route('packages.edit', $package)->with('success', 'package copied.');
    }
}
