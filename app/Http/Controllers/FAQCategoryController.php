<?php

namespace App\Http\Controllers;

use App\FAQCategory;
use Inertia\Inertia;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;

class FAQCategoryController extends Controller
{
    function index()
    {
        $faq_categories = FAQCategory::all();
        return Inertia::render('FAQs/Categories/Index', [
            'categories' => $faq_categories
        ]);
    }

    public function create() {
        $faq_categories = FAQCategory::all();
        return Inertia::render('FAQs/Categories/Create', [
            'categories' => $faq_categories
        ]);
    }

    public function store() {
        logger(request()->all());
        $categoryDetails = Request::validate([
            'label' => ['required', 'unique:faq_categories'],
        ]);
        $categoryDetails['slug'] = str_slug($categoryDetails['label']);
        $user = FAQCategory::create($categoryDetails);
        return Redirect::route('faqs')->with('success', 'Category created.');
    }

    public function edit(FAQCategory $category) {
        return Inertia::render('FAQs/Categories/Edit', [
            'category' => $category
        ]);
    }

    public function update(FAQCategory $category) {
        $categoryDetails = Request::validate([
            'label' => ['required', Rule::unique('faq_categories')->ignore($category->id)],
        ]);
        $category->update( $categoryDetails );
        return Redirect::route('faq-categories.edit', $category)->with('success', 'Category updated.');
    }
}

