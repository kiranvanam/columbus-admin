<?php

namespace App\Http\Controllers;

use App\Country;
use App\Destination;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class DestinationsController extends Controller
{
    public function index() {
        $servingCountries = Country::where('serving', true)->get();
        return Inertia::render('Destinations/Index', [
            'filters' => Request::all('search', 'serving'),
            'servingCountries' => $servingCountries,
            'destinations' => Destination::all()
        ]);
    }

    public function create() {
        $selectedCountry = count(Request::only('country')) ? Country::find(Request::only('country')['country']) : null;
        $servingCountries = Country::where('serving', true)->get();
        return Inertia::render('Destinations/Create', [
            'servingCountries' => $servingCountries,
            'selectedCountry' => $selectedCountry
        ]);
    }

    public function store() {
        $destinationDetails = Request::validate([
            'country_code' => ['required'],
            'name' => ['required'],
            'code' => ['required', 'max:20'],
            'type' => ['required'],
            'searchable_terms' => ['nullable'],
            'lat' => ['nullable'],
            'lng' => ['nullable'],
        ]);

        $destinationDetails['slug'] = str_slug($destinationDetails['name']);
        Destination::create($destinationDetails);
        return Redirect::route('destinations')->with('success', 'Destination created.');
    }

    public function edit(Destination $destination) {
        $country = Country::where('cca2', $destination->country_code)->get()[0];
        return Inertia::render('Destinations/Edit', [
            'destination' => [
                'id' => $destination->id,
                'name' => $destination->name,
                'code' => $destination->code,
                'type' => $destination->type,
                'searchable_terms' => $destination->searchable_terms,
                'lat' => $destination->lat,
                'lng' => $destination->lng,
                'deleted_at' => $destination->deleted_at,
                'country_name' => $country->name
            ],
        ]);
    }

    public function update(Destination $destination) {
        $destinationDetails = Request::validate([
            'name' => ['required'],
            'code' => ['required', 'max:20'],
            'type' => ['required'],
            'searchable_terms' => ['nullable'],
            'lat' => ['nullable'],
            'lng' => ['nullable'],
        ]);
        $destinationDetails['slug'] = str_slug($destinationDetails['name']);

        $destination->update($destinationDetails);

        return Redirect::route('destinations.edit', $destination)->with('success', "Destination updated");
    }

    public function destroy(Destination $destination)
    {
        $destination->delete();

        return Redirect::route('destinations.edit', $destination)->with('success', 'Destination deleted.');
    }

    public function restore(Destination $destination)
    {
        $destination->restore();

        return Redirect::route('destinations.edit', $destination)->with('success', 'Destination restored.');
    }

    public function getDestinations() {
        $searchQuery = request('query', '');
        if(empty($searchQuery)) {
            return [];
        }
        $destinations = Destination::with('country')->filter($searchQuery)->get();
        return $destinations;
    }
}