<?php

namespace App\Http\Controllers;

use App\Filter;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class FiltersController extends Controller
{
    public function index () {
        $settingFilters = Filter::with('parent')->get();
        return Inertia::render('Filters/Index', [
            'filters' => Request::all('search', 'serving'),
            'settingFilters' => $settingFilters
        ]);
    }

    public function create(Filter $filter) {
        $filters = Filter::whereNull('parent_id')->get();
        return Inertia::render('Filters/Edit', [
            'filters' => $filters
        ]);
    }

    public function store() {
        $filter = Filter::create($this->getFilterDetails());
        return Redirect::route('settings.filters.edit', $filter)->with('success', "Filter created.");
    }

    public function edit(Filter $filter) {
        return Inertia::render('Filters/Edit', [
            'filter' => [
                'id' => $filter->id,
                'name' => $filter->name,
                'parent_id' => $filter->parent_id,
                'filter_type' => $filter->filter_type,
                'description' => $filter->description,
                'deleted_at' => $filter->deleted_at
            ],
        ]);
    }

    public function update(Filter $filter) {
        $filter->update($this->getFilterDetails());
        return Redirect::route('settings.filters.edit', $filter)->with('success', "Filter updated");
    }

    public function destroy(Filter $filter)
    {
        $filter->delete();
        return Redirect::route('settings.filters.edit', $filter)->with('success', 'Filter deleted.');
    }

    public function restore(Filter $filter)
    {
        $filter->restore();
        return Redirect::route('settings.filters.edit', $filter)->with('success', 'Filter restored.');
    }

    public function getFilterDetails() {
        $filterDetails = Request::validate([
            'name' => ['required'],
            'filter_type' => ['required'],
            'parent_id' => ['nullable'],
            'description' => ['nullable']
        ]);
        $filterDetails['slug'] = str_slug($filterDetails['name']);
        return $filterDetails;
    }
}