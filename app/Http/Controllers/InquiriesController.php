<?php

namespace App\Http\Controllers;

use App\Code;
use App\Contact;
use App\User;
use App\Inquiry;
use App\Status;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class InquiriesController extends Controller
{
    public function index() {

        $filterValues = Request::only('search', 'trashed', 'assigned_to', 'status_ids');
        //$filterValues['status_id'] = Request::input('status');
        if( empty($filterValues['assigned_to']))
            $filterValues['assigned_to'] = \Auth::id();

        $inquiries = Inquiry::with(['customer', 'assignedTo'])
                            ->filter($filterValues)
                            ->orderBy('created_at', 'desc')
                            ->paginate();

        $filters = Request::all('search', 'trashed', 'assigned_to', 'status');
        if($filters['assigned_to']) {
            $filters['assigned_to'] = User::find($filters['assigned_to']);
        }
        return Inertia::render('Inquiries/Index', [
            'filters' => $filters,
            'inquiries' => $inquiries,
            'statuses' => Status::all()
        ]);
    }

    public function create() {
        $statuses = Status::all();
        return Inertia::render('Inquiries/Create', [
            'statuses' => $statuses
        ]);
    }

    public function store() {
        $requestDetails = Request::validate([
            'name' => ['required', 'max:100'],
            //'code' => ['required', 'max:20'],
            'date_of_travel' => ['nullable'],
            'package' => ['nullable', 'max:20'],
            'status_id' => ['required'],
            'notes.*.notes' => ['required'],
            'notes.*.date' => ['required'],
            'notes.*.user' => ['required'],
            'created_by_customer' => ['nullable'],
            'customer_id' => [ 'required_without:first_name'],
            'first_name' => ['required_without:customer_id'],
            //'phone' =>  ['required_without:customer_id'],
            'assigned_to' => ['nullable'],
            'inquiry_type' => ['required'],
            'lead_source_type' => ['nullable', 'required_without:customer_id'],
            'lead_source_name' => ['nullable', 'required_without:customer_id'],
            'lead_source_user_id' => ['nullable', 'required_without:customer_id'],
            'priority' => ['required'],
        ]);

        //if new customer create customer first
        if( !empty(request('first_name')) ) {
            $customer_data = request()->only(['title', 'first_name', 'last_name', 'phone', 'email']);
            $contact = User::addCustomer($customer_data);
            $requestDetails['customer_id'] = $contact->id;
        }

        // Set the created_by to the current user
        $requestDetails['code'] = Code::nextCodeForInquiry();
        $requestDetails['created_by'] = \Auth::id();
        if(is_null($requestDetails['assigned_to'])) {
            // If the inquiry is not assigned to anybody, assign it to the creator
            $requestDetails['assigned_to'] = $requestDetails['created_by'];
        }

        $inquiry = Inquiry::create($requestDetails);

        \Illuminate\Support\Facades\Notification::send(
                                                    $inquiry,
                                                    new \App\Notifications\InquiryCreated()
                                                );
        return Redirect::route('inquiries')->with('success', 'Inquiry created.');
    }

    public function edit(Inquiry $inquiry)
    {
        $statuses = Status::all();
        $customer = $inquiry->customer;
        $assignedTo = $inquiry->assignedTo;
        $createdBy = $inquiry->createdBy;

        return Inertia::render('Inquiries/Edit', [
            'inquiry' => $inquiry,
            'statuses' => $statuses,
            'customer' => $customer,
            'assignedTo' => $assignedTo,
            'createdBy' => $createdBy
        ]);
    }

    public function update(Inquiry $inquiry)
    {
        $updatedDetails = Request::validate([
            'name' => ['required', 'max:100'],
            'code' => ['required', 'max:20'],
            'date_of_travel' => ['nullable'],
            'package' => ['nullable', 'max:20'],
            'status_id' => ['required'],
            'notes.*.notes' => ['required'],
            'notes.*.date' => ['required'],
            'notes.*.user' => ['required'],
            'created_by_customer' => ['nullable'],
            'customer_id' => ['nullable'],
            'assigned_to' => ['nullable'],
            'inquiry_type' => ['required'],
            'priority' => ['required'],
        ]);

        $inquiry->update($updatedDetails);

        return Redirect::route('inquiries', $inquiry)->with('success', 'Inquiry updated.');
    }

    public function destroy(Inquiry $inquiry)
    {
        $inquiry->delete();

        return Redirect::route('inquiries.edit', $inquiry)->with('success', 'Inquiry deleted.');
    }

    public function restore(Inquiry $inquiry)
    {
        $inquiry->restore();

        return Redirect::route('inquiries.edit', $inquiry)->with('success', 'Inquiry restored.');
    }

    public function groupByStatus()
    {
        $used_id = request('user_id');
        if( empty($used_id) )
            $used_id = \Auth::id();

        $groupByCount = Inquiry::select(\DB::raw('count(*) as total, status_id'))
                            ->where('assigned_to', $used_id)
                            ->groupBy('status_id')
                            ->get()
                            ->toArray();

        $groupByCount = array_map( function($statusCount) {
                              $statusCount['status'] = Status::where( 'id', $statusCount['status_id'])->first()->name;
                              return $statusCount;
                            },  $groupByCount);
        return $groupByCount;
    }
}
