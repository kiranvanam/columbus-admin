<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DBBackupController extends Controller
{
    function index()
    {
        $fileName = now()->format('Y-m-d_H-i-s');
        \Artisan::call("snapshot:create", ['name' => $fileName]);

        return \Storage::disk('snapshots')->download($fileName . ".sql");
    }
}
