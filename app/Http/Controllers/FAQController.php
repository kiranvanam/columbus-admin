<?php

namespace App\Http\Controllers;

use App\FAQ;
use App\FAQCategory;
use Inertia\Inertia;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

class FAQController extends Controller
{
    function index()
    {
        $faqs = FAQ::all();
        $faq_categories = FAQCategory::all();
        return Inertia::render('FAQs/Index', [
            'faqs' => $faqs,
            'categories' => $faq_categories
        ]);
    }

    public function create() {
        $faqs = FAQ::all();
        $faq_categories = FAQCategory::all();
        return Inertia::render('FAQs/Create', [
            'faqs' => $faqs,
            'categories' => $faq_categories
        ]);
    }

    public function store() {
        $faqDetails = Request::validate([
            'question' => ['required'],
            'answer' => ['required'],
            'categories' => ['required', 'array']
        ]);
        $user = FAQ::create($faqDetails);
        return Redirect::route('faqs')->with('success', 'FAQ created.');
    }

    public function edit(FAQ $faq) {
        $faq_categories = FAQCategory::all();
        return Inertia::render('FAQs/Edit', [
            'categories' => $faq_categories,
            'faq' => $faq
        ]);
    }

    public function update(FAQ $faq) {
        $faqDetails = Request::validate([
            'question' => ['required'],
            'answer' => ['required'],
            'categories' => ['required', 'array']
        ]);
        $faq->update( $faqDetails );
        return Redirect::route('faqs.edit', $faq)->with('success', 'FAQ updated.');

    }
}
