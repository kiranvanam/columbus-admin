<?php

namespace App\Http\Controllers;

use App\Account;
use App\User;
use App\Country;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class ContactsController extends Controller
{
    public function index() {
        $contacts = User::filter(Request::only('search', 'trashed'))
                        ->where('user_type', 'customer')
                        /* ->with('account') */
                        ->paginate();

        return Inertia::render('Contacts/Index', [
            'filters' => Request::all('search', 'trashed'),
            'contacts' => $contacts,
        ]);
    }

    public function create() {
        $selectedAccount = count(Request::only('account')) ? Account::find(Request::only('account')['account']) : null;
        $countries = Country::all();
        return Inertia::render('Contacts/Create', [
            'countries' => $countries,
            'selectedAccount' => $selectedAccount
        ]);
    }

    public function store() {
        logger("Loooooooooooooging store:User");
        $contactDetails = $this->validateRequest();
        User::addCustomer($contactDetails);
        return Redirect::route('contacts')->with('success', 'Contact created.');
    }

    public function edit(User $contact)
    {
        $countries = Country::all();
        $account = Account::find($contact->account_id);
        return Inertia::render('Contacts/Edit', [
            'contact' => [
                'id' => $contact->id,
                'title' => $contact->title,
                'first_name' => $contact->first_name,
                'last_name' => $contact->last_name,
                'email' => $contact->email,
                'phone' => $contact->phone,
                'address' => $contact->address,
                'city' => $contact->city,
                'region' => $contact->region,
                'country' => $contact->country,
                'postal_code' => $contact->postal_code,
                'deleted_at' => $contact->deleted_at,
                /* 'dob' => $contact->dob,
                'role' => $contact->role,
                'account_id' =>$contact->account_id,
                'alt_email' => $contact->alt_email,
                'alt_phone' => $contact->alt_phone,
                'account' => $account */
            ],
            'countries' => $countries
        ]);
    }

    public function update(User $contact)
    {
        $contactDetails = $this->validateRequest();
        $contact->update($contactDetails);
        return Redirect::route('contacts.edit', $contact)->with('success', 'Contact updated.');
    }

    public function destroy(User $contact)
    {
        $contact->delete();
        return Redirect::route('contacts.edit', $contact)->with('success', 'Contact deleted.');
    }

    public function restore(User $contact)
    {
        $contact->restore();
        return Redirect::route('contacts.edit', $contact)->with('success', 'Contact restored.');
    }

    protected function validateRequest() {
        $details = Request::validate([
            'title' => ['required', 'max:20'],
            'first_name' => ['required', 'max:100'],
            'last_name' => ['required', 'max:100'],
            'email' => ['nullable', 'email', 'required_without:phone'],
            'phone' => ['nullable', 'required_without:email'],
            'city' => ['nullable', 'max:50'],
            'region' => ['nullable', 'max:50'],
            'country' => ['nullable', 'max:2'],
            'postal_code' => ['nullable', 'max:25'],
            'address' => ['nullable', 'max:150'],
            /* 'dob' => ['nullable'],
            'account_id' => ['required'],
            'alt_email' => ['nullable', 'email'],
            'alt_phone' => ['nullable'],
            'role' => ['nullable', 'max:100'],
            */
        ]);

        if( ! isset( $details['phone'] ) )
            unset($details['phone']);

        if( ! isset( $details['city'] ) )
            unset($details['city']);

        if( ! isset( $details['region'] ) )
            unset($details['region']);

        if( ! isset( $details['country'] ) )
            unset($details['country']);

        if( ! isset( $details['postal_code'] ) )
            unset($details['postal_code']);

        if( ! isset( $details['address'] ) )
            unset($details['address']);


        return $details;
    }

    public function getContacts()
    {
        $searchQuery = request('query', '');
        if(empty($searchQuery)) {
            return [];
        }
        $contacts = User::where('first_name', 'like', '%'.$searchQuery.'%')
                            ->orWhere('last_name', 'like', '%'.$searchQuery.'%')->get();
        return $contacts;
    }
}
