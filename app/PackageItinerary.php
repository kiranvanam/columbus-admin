<?php

namespace App;

class PackageItinerary extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'meals' => 'array',
        'additional_details' => 'array'
    ];

    public function package() {
        return $this->belongsTo(Package::class);
    }
}
