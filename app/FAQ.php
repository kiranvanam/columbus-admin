<?php

namespace App;

class FAQ extends Model
{
    protected $table = "faqs";

    protected $guarded = [];

    /**
     * Get the owning faqable model.
     */
    public function faqable ()
    {
        return $this->morphTo();
    }
}