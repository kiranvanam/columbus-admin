<?php

namespace App;

class FAQCategory extends Model
{
    protected $table = "faq_categories";

    protected $fillable = ['label', 'slug'];

    public function faqs() {
        return $this->morphMany(FAQ::class, 'faqable');
    }
}