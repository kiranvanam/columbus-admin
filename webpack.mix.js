const mix = require('laravel-mix');
const tailwind = require('tailwindcss');
const path = require('path');
let postCssImport = require('postcss-import');


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/backend/app.js', 'public/js/backend')
    .js('resources/js/website/app.js', 'public/js/website')
    .postCss('resources/css/backend/app.css', 'public/css/backend', [
        tailwind('./backend.tailwind.config.js'),
    ])
    .postCss('resources/css/website/app.css', 'public/css/website', [
      postCssImport(),
      tailwind('./website.tailwind.config.js'),
    ])
    .autoload({ jquery: ['$', 'window.jQuery', 'jQuery'] })
    .webpackConfig({
        output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
        resolve: {
          alias: {
            vue$: 'vue/dist/vue.runtime.esm.js',
            '@': path.resolve('resources/js'),
          },
        },
    });