<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('login')->name('login')->uses('Auth\LoginController@showLoginForm');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');
Route::post('logout')->name('logout')->uses('Auth\LoginController@logout');

// Dashboard
Route::get('/')->name('dashboard')->uses('DashboardController')->middleware('auth');

// Upload File
Route::post('/upload')->name('upload')->uses('FileUploadController@upload');
Route::delete('/delete')->name('delete')->uses('FileUploadController@delete');

// Inquiries
Route::get('/inquiries')->name('inquiries')->uses('InquiriesController@index');
Route::get('inquiries/create')->name('inquiries.create')->uses('InquiriesController@create');
Route::post('inquiries')->name('inquiries.store')->uses('InquiriesController@store');
Route::get('inquiries/{inquiry}/edit')->name('inquiries.edit')->uses('InquiriesController@edit');
Route::put('inquiries/{inquiry}')->name('inquiries.update')->uses('InquiriesController@update');
Route::delete('inquiries/{inquiry}')->name('inquiries.destroy')->uses('InquiriesController@destroy');
Route::put('inquiries/{inquiry}/restore')->name('inquiries.restore')->uses('InquiriesController@restore');
Route::get('inquiries/group-by-status')->name('inquiries.group-by-status')->uses('InquiriesController@groupByStatus');
//Users
Route::get('/users')->name('users')->uses('UsersController@index');
Route::get('users/create')->name('users.create')->uses('UsersController@create');
Route::post('users')->name('users.store')->uses('UsersController@store');
Route::get('users/{user}/edit')->name('users.edit')->uses('UsersController@edit');
Route::put('users/{user}')->name('users.update')->uses('UsersController@update');
Route::delete('users/{user}')->name('users.destroy')->uses('UsersController@destroy');
Route::put('users/{user}/restore')->name('users.restore')->uses('UsersController@restore');
//Users API
Route::get('/api/users')->name('api.users')->uses('UsersController@getUsers');

// Countries
Route::get('/countries')->name('countries')->uses('CountriesController@index');
Route::get('/countries/{country}/edit')->name('countries.edit')->uses('CountriesController@edit');
Route::put('countries/{country}')->name('countries.update')->uses('CountriesController@update');
// Countries API
Route::get('/api/countries')->name('api.countries')->uses('CountriesController@getCountries');

// Destinations
Route::get('/destinations')->name('destinations')->uses('DestinationsController@index');
Route::get('destinations/create')->name('destinations.create')->uses('DestinationsController@create');
Route::post('destinations')->name('destinations.store')->uses('DestinationsController@store');
Route::get('/destinations/{destination}/edit')->name('destinations.edit')->uses('DestinationsController@edit');
Route::put('destinations/{destination}')->name('destinations.update')->uses('DestinationsController@update');
Route::delete('destinations/{destination}')->name('destinations.destroy')->uses('DestinationsController@destroy');
Route::put('destinations/{destination}/restore')->name('destinations.restore')->uses('DestinationsController@restore');
// Destinations API
Route::get('/api/destinations')->name('api.destinations')->uses('DestinationsController@getDestinations');

// Settings
Route::group(['prefix' => 'settings'], function () {
    Route::get('packages')->name('settings.packages')->uses('PackageSettingsController@index');
    Route::get('packages/create')->name('settings.packages.create')->uses('PackageSettingsController@create');
    Route::get('packages/{package_setting}/edit')->name('settings.packages.edit')->uses('PackageSettingsController@edit');
    Route::post('packages')->name('settings.packages.store')->uses('PackageSettingsController@store');
    Route::put('packages/{package_setting}')->name('settings.packages.update')->uses('PackageSettingsController@update');

    Route::group(['prefix' => 'filters'], function () {
        Route::get('/')->name('settings.filters')->uses('FiltersController@index');
        Route::get('create')->name('settings.filters.create')->uses('FiltersController@create');
        Route::get('edit/{filter}')->name('settings.filters.edit')->uses('FiltersController@edit');
        Route::post('/')->name('settings.filters.store')->uses('FiltersController@store');
        Route::put('/{filter}')->name('settings.filters.update')->uses('FiltersController@update');
        Route::delete('{filter}')->name('settings.filters.destroy')->uses('FiltersController@destroy');
        Route::put('restore/{filter}')->name('settings.filters.restore')->uses('FiltersController@restore');
    });
});

// Collections
Route::get('/collections')->name('collections')->uses('CollectionsController@index');
Route::get('collections/create')->name('collections.create')->uses('CollectionsController@create');
Route::post('collections')->name('collections.store')->uses('CollectionsController@store');
Route::get('/collections/{collection}/edit')->name('collections.edit')->uses('CollectionsController@edit');
Route::put('collections/{collection}')->name('collections.update')->uses('CollectionsController@update');
Route::delete('collections/{collection}')->name('collections.destroy')->uses('CollectionsController@destroy');
Route::put('collections/{collection}/restore')->name('collections.restore')->uses('CollectionsController@restore');

// Packages
Route::get('/packages')->name('packages')->uses('PackagesController@index');
Route::get('packages/create')->name('packages.create')->uses('PackagesController@create');
Route::post('packages')->name('packages.store')->uses('PackagesController@store');
Route::get('/packages/{package}')->name('packages.edit')->uses('PackagesController@edit');
Route::put('packages/{package}')->name('packages.update')->uses('PackagesController@update');
Route::get('/packages/{package}/gallery')->name('packages.gallery')->uses('PackagesController@editGallery');
Route::put('/packages/{package}/gallery')->name('packages.update.gallery')->uses('PackagesController@updateGallery');
Route::get('/packages/{package}/destinations')->name('packages.destinations')->uses('PackagesController@editDestinations');
Route::put('/packages/{package}/destinations')->name('packages.update.destinations')->uses('PackagesController@updateDestinations');
Route::get('/packages/{package}/itinerary')->name('packages.itinerary')->uses('PackagesController@editItinerary');
Route::put('/packages/{package}/itinerary')->name('packages.update.itinerary')->uses('PackagesController@updateItinerary');
Route::get('/packages/{package}/pricing')->name('packages.pricing')->uses('PackagesController@edit');
Route::get('/packages/{package}/information')->name('packages.information')->uses('PackagesController@editInformation');
Route::put('/packages/{package}/information')->name('packages.update.information')->uses('PackagesController@updateInformation');
Route::get('/packages/{package}/accommodations')->name('packages.accommodations')->uses('PackagesController@editAccommodations');
Route::put('/packages/{package}/accommodations')->name('packages.update.accommodations')->uses('PackagesController@updateAccommodations');
Route::get('/packages/{package}/filters')->name('packages.filters')->uses('PackagesController@editFilters');
Route::put('/packages/{package}/filters')->name('packages.update.filters')->uses('PackagesController@updateFilters');
Route::get('/packages/{package}/faqs')->name('packages.faqs')->uses('PackagesController@editFAQs');
Route::put('/packages/{package}/faqs')->name('packages.update.faqs')->uses('PackagesController@updateFAQs');
Route::delete('packages/{package}')->name('packages.destroy')->uses('PackagesController@destroy');
Route::put('packages/{package}/restore')->name('packages.restore')->uses('PackagesController@restore');
Route::put('packages/{package}/clone')->name('packages.clone')->uses('PackagesController@clone');

// Accounts
Route::get('/accounts')->name('accounts')->uses('AccountsController@index');
Route::get('accounts/create')->name('accounts.create')->uses('AccountsController@create');
Route::post('accounts')->name('accounts.store')->uses('AccountsController@store');
Route::get('accounts/{account}/edit')->name('accounts.edit')->uses('AccountsController@edit');
Route::put('accounts/{account}')->name('accounts.update')->uses('AccountsController@update');
Route::delete('accounts/{account}')->name('accounts.destroy')->uses('AccountsController@destroy');
Route::put('accounts/{account}/restore')->name('accounts.restore')->uses('AccountsController@restore');
// Accounts API
Route::get('/api/accounts')->name('api.accounts')->uses('AccountsController@getAccounts');

// Contacts
Route::get('/contacts')->name('contacts')->uses('ContactsController@index');
Route::get('contacts/create')->name('contacts.create')->uses('ContactsController@create');
Route::post('contacts')->name('contacts.store')->uses('ContactsController@store');
Route::get('contacts/{contact}/edit')->name('contacts.edit')->uses('ContactsController@edit');
Route::put('contacts/{contact}')->name('contacts.update')->uses('ContactsController@update');
Route::delete('contacts/{contact}')->name('contacts.destroy')->uses('ContactsController@destroy');
Route::put('contacts/{contact}/restore')->name('contacts.restore')->uses('ContactsController@restore');
// Contacts API
Route::get('/api/contacts')->name('api.contacts')->uses('ContactsController@getContacts');

Route::group(["prefix" => "faqs"], function()
{
    Route::get('')->name('faqs')->uses('FAQController@index');
    Route::get('create')->name("faqs.create")->uses('FAQController@create');
    Route::post('create')->name("faqs.store")->uses('FAQController@store');
    Route::get("{faq}/edit")->name("faqs.edit")->uses('FAQController@edit');
    Route::put("{faq}/update")->name("faqs.update")->uses('FAQController@update');
});

Route::group(['prefix'=>"faq-categories"],function(){
    Route::get('create')->name('faq-categories.create')->uses('FAQCategoryController@create');
    Route::post('create')->name('faq-categories.store')->uses('FAQCategoryController@store');
    Route::get('{category}/edit')->name('faq-categories.edit')->uses('FAQCategoryController@edit');
    Route::put('{category}/update')->name('faq-categories.update')->uses('FAQCategoryController@update');
});

Route::group(["prefix" => "codes"], function() {
    Route::get("")->name("codes")->uses("CodeGenerationController@index");
    Route::post("")->name("codes.store")->uses("CodeGenerationController@store");
    Route::get("/{module_name}/next")->name("codes.next")->uses("CodeGenerationController@nextCodeForModule");
});

// Route::group(["prefix" => "sms", 'namespace' => 'Notifications'], function() {
//     Route::get("promotions")->name('sms-promotions')->uses('NotificationController@index');
//     Route::get("sms-notifications")->name("sms-notifications.create")->uses("SMSController@create");
// });
/*
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/

Route::group([ 'middleware' => 'auth'], function() {
    Route::get("statuses-seed", function() {
        \Artisan::call("db:seed  --class='StatusesTableSeeder'");
    });
    Route::group(['prefix' => 'downloads'], function() {
        Route::get('db-backup', 'DBBackupController@index')->name('db-backup');

        Route::get("contact-numbers", function() {
            return \Excel::download(new \App\Exports\UserContact, "customer-contacts.xlsx");
        })->name("download-contact-numbers");
    });

    Route::get("db-trim-user-phone", function() {
        \App\User::all()->each(function($user) {
            $user->phone = trim($user->phone);
            $user->save();
        });
    });
});

