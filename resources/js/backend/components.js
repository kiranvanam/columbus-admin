import Vue from 'vue'
import VueMultiselect from 'vue-multiselect'
import TinyEditor from '@tinymce/tinymce-vue'

import CheckBox from '@/Shared/CheckBox'
import DateLabel from '@/Shared/DateLabel'
import Dropdown from '@/Shared/Dropdown'
import FileUploader from '@/Shared/FileUploader'
import FlashMessages from '@/Shared/FlashMessages'
import Icon from '@/Shared/Icon'
import Layout from '@/Shared/Layout'
import LoadingButton from '@/Shared/LoadingButton'
import MainMenu from '@/Shared/MainMenu'
import Modal from '@/Shared/Modal'
import ModuleCode from '@/Pages/Codes/InitializeCode'
import MultiselectInput from '@/Shared/MultiselectInput'
import Pagination from '@/Shared/Pagination'
import ProgressBar from '@/Shared/ProgressBar'
import Radio from '@/Shared/Radio'
import SearchFilter from '@/Shared/SearchFilter'
import SelectInput from '@/Shared/SelectInput'
import Settings from '@/Shared/Settings'
import SubmitButton from '@/Shared/SubmitButton'
import Textareainput from '@/Shared/Textareainput'
import TextInput from '@/Shared/TextInput'
import TrashedMessage from '@/Shared/TrashedMessage'
import UserTitle from '@/Shared/UserTitle'
import UserSelect from '@/Shared/UserSelect'
import VueEditor from '@/Shared/VueEditor'


Vue.component('vue-multiselect', VueMultiselect)
Vue.component('tiny-editor', TinyEditor)

Vue.component('check-box', CheckBox)
Vue.component('date-label', DateLabel)
Vue.component('dropdown', Dropdown)
Vue.component('file-uploader', FileUploader)
Vue.component('flash-messages', FlashMessages)
Vue.component('icon', Icon)
Vue.component('layout', Layout)
Vue.component('loading-button', LoadingButton)
Vue.component('main-menu', MainMenu)
Vue.component('modal', Modal)
Vue.component('module-code', ModuleCode)
Vue.component('multiselect-input', MultiselectInput)
Vue.component('pagination', Pagination)
Vue.component('progress-bar', ProgressBar)
Vue.component('radio', Radio)
Vue.component('search-filter', SearchFilter)
Vue.component('select-input', SelectInput)
Vue.component('settings', Settings)
Vue.component('submit-button', SubmitButton)
Vue.component('textareainput', Textareainput)
Vue.component('text-input', TextInput)
Vue.component('trashed-message', TrashedMessage)
Vue.component('user-title', UserTitle)
Vue.component('user-select', UserSelect)
Vue.component('vue-editor', VueEditor)