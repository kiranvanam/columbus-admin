require('../bootstrap.js');

import { InertiaApp } from '@inertiajs/inertia-vue'
import Vue from 'vue'

Vue.use( InertiaApp )

import PortalVue from 'portal-vue'
Vue.use(PortalVue)

import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {});

import Helpers from './mixins'
Vue.mixin(Helpers)

require('./components.js');

const app = document.getElementById('app')

new Vue({
  render: h => h(InertiaApp, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => import(`@/Pages/${name}`).then(module => module.default),
    },
  }),
}).$mount(app)