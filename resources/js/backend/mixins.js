export default {
    computed: {
        inquiryStatuses() {
            return this.statuses ? this.statuses: []
        }
    },
    methods: {
        route: (...args) => {
            return window.route(...args).url();
        },
        routeUri: (...args) => {
            return window.route(...args).url().slice(window.Ziggy.baseUrl.length - 1);
        },
        isCurrentUrl: (url) => {
            let path = url.replace(location.origin, '');
            return path === location.pathname;
        },
        getFullContactName: (user) => {
            return user  ? `${user.first_name ? user.first_name : ''} ${user.last_name ? user.last_name : ''}` : '';
        },
        getFullEmployeeName: (user) => {
            return user  ? `${user.first_name ? user.first_name : ''} ${user.last_name ? user.last_name : ''}` : '';
        },
        getUserFullName: (user) => {
            return user  ? `${user.first_name ? user.first_name : ''} ${user.last_name ? user.last_name : ''}` : '';
        },
        getUserContactDetails(user) {
            let phone=this.getUserPhone(user), email=this.getUserEmail(user)
            /* if( phone == email || !email )
                email = 'No Email'
            else if (!phone) {
                phone = 'No Phone'
            } */
            return `${phone} - ${email}`
        },
        getUserPhone(user) {
            return user.phone ? user.phone : 'No phone'
        },
        getUserEmail(user) {
            if( user.phone === user.email || !user.email )
                return 'No Email'
            return user.email
        },
        statusById(id) {
            const status = this.inquiryStatuses.find( (status) => status.id == id)
            return status ? status : { name: 'Invalid status' }
        },
        copyToClipBoard(elementId) {

            var copyText = document.getElementById(elementId);
            const selection = window.getSelection();
            const range = document.createRange();
            range.selectNodeContents(copyText);
            selection.removeAllRanges();
            selection.addRange(range);
            document.execCommand('copy');
        }
    }
}