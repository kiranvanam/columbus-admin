<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href="{{ mix('/css/backend/app.css') }}" rel="stylesheet">
    <script src="{{ mix('/js/backend/app.js') }}" defer></script>
    <script src="https://kit.fontawesome.com/0110a9e37b.js" defer></script>
    <script src="https://unpkg.com/dayjs"></script>
    @routes
</head>
<body>

@inertia

</body>
</html>